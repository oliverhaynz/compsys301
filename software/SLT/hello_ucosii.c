// libraries
#include <stdio.h>
#include <time.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_pwm_regs.h"
#include "altera_avalon_pwm_routines.h"
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include "adc_main.h"
#include "cmd.h"

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    task1_stk[TASK_STACKSIZE];
OS_STK    task2_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
#define TASK2_PRIORITY      2

int t1,t2,t3,t4,t5,t6,a=800,b=800,flag7,flag8,t7,t8;
/* Prints "Hello World" and sleeps for three seconds */
void task1(void* pdata)
{
	double val = 0, val2 = 0;
	double val1 = 0, val3 = 0;
//	int a=800,b=800;
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);
	IOWR_ALTERA_AVALON_PWM_MODE(PWM0_BASE,0);
	IOWR_ALTERA_AVALON_PWM_MODE(PWM1_BASE,0);
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM0_BASE, 5);
	//Set the duty cycle to a (currently 0)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, 600);
	//Set the initial PWM1 clock divider=
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM1_BASE, 5);
	//Set the duty cycle to a (currently 0 also)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, 600);
	while (1)
	{
		val =  adc_read(0b000)/4096.0; //PWM1
		val1 =  adc_read(0b001)/4096.0; //PWM0
		val2 =  adc_read(0b010)/4096.0; //PWM1
		val3 =  adc_read(0b011)/4096.0; //PWM0

		if(val > 0.65){
			t1 = clock();
			t3 = t1 - t2;		//Get the time period for option 1 read
			flag7 = 0;			//Option 2 using flags increment flag on each high.
		}						//1 flag = 55mm (Could implement 27.5 instead)
		else if(val2 > 0.65){
			flag7 = 0;
		}
		else{
			if(!flag7){
				t7++;			//Increment flag here
			}
			t2 = t1;
			flag7 = 1;
		}

		if(val1 > 0.65){
			t4 = clock();
			t6 = t4 - t5;
			flag8 = 0;
		}
		else if(val3 > 0.65){
			flag8 = 0;
		}
		else{
			if(!flag8){
				t8++;
			}
			t5 = t4;
			flag8 = 1;
		}

		IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, a);
		IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, b);
		OSTimeDlyHMSM(0, 0, 0, 5);
	}
}
/* Prints "Hello World" and sleeps for three seconds */
void task2(void* pdata)
{
  while (1)
  { 
	  if(t8 < t7){
	  	  a=800;
	  	  b=0;
	  }else if(t8 == t7){
		  a=800;
		  b=800;
	  }
	  else{
		  b=800;
		  a=0;
	  }
    OSTimeDlyHMSM(0, 0, 0, 100);
    printf("%d\t%d\n",t7,t8);
  }
}
/* The main function creates two task and starts multi-tasking */
int main(void)
{
  
  OSTaskCreateExt(task1,
                  NULL,
                  (void *)&task1_stk[TASK_STACKSIZE-1],
                  TASK1_PRIORITY,
                  TASK1_PRIORITY,
                  task1_stk,
                  TASK_STACKSIZE,
                  NULL,
                  0);
              
               
  OSTaskCreateExt(task2,
                  NULL,
                  (void *)&task2_stk[TASK_STACKSIZE-1],
                  TASK2_PRIORITY,
                  TASK2_PRIORITY,
                  task2_stk,
                  TASK_STACKSIZE,
                  NULL,
                  0);
  OSStart();
  return 0;
}
