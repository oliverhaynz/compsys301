/*
 * cmd.h
 *
 *  Created on: 7/08/2015
 *      Author: OEM
 */

#ifndef CMD_H_
#define CMD_H_
#define INSTRUCTION_SIZE 20

//Forward = 0, Right = 1, Left = 2, Backwards = 3, stop = 4, 10, slight left, 20, slight right
int INSTRUCTION_CMD[INSTRUCTION_SIZE] = {5,5,5,5,5,5,5,5,5,5,
										 5,5,5,5,5,5,5,5,5,5};

//int INSTRUCTION_CMD[INSTRUCTION_SIZE] = {4,4,4,4,4,4,4,4,4,4,
//										 4,4,4,4,4,4,4,4,4,4};

#endif /* CMD_H_ */
