
#include "altera_avalon_encoder_regs.h"

#define ALTERA_AVALON_ENCODER_TYPE (volatile unsigned int*)

int encoder_position(unsigned int address);
int encoder_speed(unsigned int address);
int encoder_dir(unsigned int address);

int encoder_prescale_emulator_write(unsigned int address, unsigned int vw);
int encoder_prescale_emulator_read(unsigned int address);

int encoder_prescale_write(unsigned int address, unsigned int prescale);
int encoder_prescale_read(unsigned int address);

void encoder_reset(unsigned int address);

int encoder_pos_limit_write(unsigned int address, int limit);
int encoder_pos_limit_read(unsigned int address);

int encoder_neg_limit_write(unsigned int address, int limit);
int encoder_neg_limit_read(unsigned int address);

void encoder_1xmode(unsigned int address);
void encoder_2xmode(unsigned int address);
void encoder_4xmode(unsigned int address);

int encoder_mode_read(unsigned int address);

//Return Codes
#define ALTERA_AVALON_ENCODER_OK                                          0
#define ALTERA_AVALON_ENCODER_REGISTER_SET_ERROR                         -1

