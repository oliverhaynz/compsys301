#include "altera_avalon_pwm_routines.h"


// NOTE: Enable and mode features disabled in VHDL ie writing to 'enable' and 'mode' has no effect

//---------------------------------------------------------------
int altera_avalon_pwm_init(unsigned int address, unsigned int clock_divider, unsigned int duty_cycle)
{
    int ret = ALTERA_AVALON_PWM_OK;

//    IOWR_ALTERA_AVALON_PWM_ENABLE(address, ~ALTERA_AVALON_PWM_ENABLE_MSK);
//    if(IORD_ALTERA_AVALON_PWM_ENABLE(address) != ALTERA_AVALON_PWM_DISABLED)
//        ret = ALTERA_AVALON_PWM_REGISTER_SET_ERROR;

    IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(address, clock_divider);
    if (IORD_ALTERA_AVALON_PWM_CLOCK_DIVIDER(address) != clock_divider)
        ret = ALTERA_AVALON_PWM_REGISTER_SET_ERROR;

    IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(address, duty_cycle);
    if (IORD_ALTERA_AVALON_PWM_DUTY_CYCLE(address) != duty_cycle)
        ret = ALTERA_AVALON_PWM_REGISTER_SET_ERROR;

    //set mode to locked antiphase
    IOWR_ALTERA_AVALON_PWM_MODE(address, ALTERA_AVALON_PWM_LOCKED_ANTIPHASE_MODE);

    return ret;
}
//---------------------------------------------------------------
//int altera_avalon_pwm_enable(unsigned int address)
//{
//  IOWR_ALTERA_AVALON_PWM_ENABLE(address, ALTERA_AVALON_PWM_ENABLE_MSK);
//
//  if(IORD_ALTERA_AVALON_PWM_ENABLE(address) != ALTERA_AVALON_PWM_ENABLED) //Confirm PWM is enabled
//  {
//  	return ALTERA_AVALON_PWM_ENABLED_CONFIRMATION_ERROR;
//  }
//  return ALTERA_AVALON_PWM_OK;
//}
////---------------------------------------------------------------
//int altera_avalon_pwm_disable(unsigned int address)
//{
//  IOWR_ALTERA_AVALON_PWM_ENABLE(address, ~ALTERA_AVALON_PWM_ENABLE_MSK);
//
//  if(IORD_ALTERA_AVALON_PWM_ENABLE(address) != ALTERA_AVALON_PWM_DISABLED)  //Confirm PWM is disabled
//  {
//  	return ALTERA_AVALON_PWM_DISABLED_CONFIRMATION_ERROR;
//  }
//  return ALTERA_AVALON_PWM_OK;
//}
//---------------------------------------------------------------
int altera_avalon_pwm_change_duty_cycle(unsigned int address, unsigned int duty_cycle)
{
    int ret = ALTERA_AVALON_PWM_OK;

    IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(address, duty_cycle);
    if (IORD_ALTERA_AVALON_PWM_DUTY_CYCLE(address) != duty_cycle)
        ret = ALTERA_AVALON_PWM_REGISTER_SET_ERROR;

  return ret;
}
//---------------------------------------------------------------
//int altera_avalon_pwm_mode(unsigned int address, unsigned int mode)
//{
//  IOWR_ALTERA_AVALON_PWM_MODE(address, mode);
//
//  if(IORD_ALTERA_AVALON_PWM_MODE(address) != mode) //
//  {
//  	return ALTERA_AVALON_PWM_MODE_CONFIRMATION_ERROR;
//  }
//  return ALTERA_AVALON_PWM_OK;
//}
////---------------------------------------------------------------
//int altera_avalon_pwm_locked_antiphase_mode(unsigned int address)
//{
//  IOWR_ALTERA_AVALON_PWM_MODE(address, ALTERA_AVALON_PWM_LOCKED_ANTIPHASE_MODE );
//
//  if(IORD_ALTERA_AVALON_PWM_MODE(address) != ALTERA_AVALON_PWM_LOCKED_ANTIPHASE_MODE)
//  {
//  	return ALTERA_AVALON_PWM_MODE_CONFIRMATION_ERROR;
//  }
//  return ALTERA_AVALON_PWM_OK;
//}
////---------------------------------------------------------------
//int altera_avalon_pwm_sign_magnitude_mode(unsigned int address)
//{
//  IOWR_ALTERA_AVALON_PWM_ENABLE(address, ALTERA_AVALON_PWM_SIGN_MAGNITUDE_MODE);
//
//  if(IORD_ALTERA_AVALON_PWM_ENABLE(address) != ALTERA_AVALON_PWM_SIGN_MAGNITUDE_MODE)  //Confirm PWM is disabled
//  {
//  	return ALTERA_AVALON_PWM_MODE_CONFIRMATION_ERROR;
//  }
//  return ALTERA_AVALON_PWM_OK;
//}
