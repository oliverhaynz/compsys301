#!/bin/sh
#
# This file was automatically generated.
#
# It can be overwritten by nios2-flash-programmer-generate or nios2-flash-programmer-gui.
#

#
# Converting SOF File: C:\Users\danie\OneDrive\University\2015S2\PROJ301\AfterWin10\stamp.sof to: "..\flash/stamp_epcs_controller.flash"
#
sof2flash --input="C:/Users/danie/OneDrive/University/2015S2/PROJ301/AfterWin10/stamp.sof" --output="../flash/stamp_epcs_controller.flash" --epcs --verbose 

#
# Programming File: "..\flash/stamp_epcs_controller.flash" To Device: epcs_controller
#
nios2-flash-programmer "../flash/stamp_epcs_controller.flash" --base=0x4005800 --epcs --sidp=0x40061B8 --id=0x13012014 --accept-bad-sysid --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program --verbose 

#
# Converting ELF File: C:\Users\danie\OneDrive\University\2015S2\PROJ301\AfterWin10\software\Main\Main.elf to: "..\flash/Main_epcs_controller.flash"
#
elf2flash --input="C:/Users/danie/OneDrive/University/2015S2/PROJ301/AfterWin10/software/Main/Main.elf" --output="../flash/Main_epcs_controller.flash" --epcs --after="../flash/stamp_epcs_controller.flash" --verbose 

#
# Programming File: "..\flash/Main_epcs_controller.flash" To Device: epcs_controller
#
nios2-flash-programmer "../flash/Main_epcs_controller.flash" --base=0x4005800 --epcs --sidp=0x40061B8 --id=0x13012014 --accept-bad-sysid --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program --verbose 

