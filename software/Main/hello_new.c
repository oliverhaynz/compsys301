/*
 ************************
 *AUTHOR: DANIEL PALEY	*
 *AUTHOR: TYRONE SHERWIN*
 ************************
 *--->Please Mark using Nios 13.1<---
 *
 */

// libraries
#include "main.h"

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    adc_task_stk[TASK_STACKSIZE];
OS_STK    light_task_stk[TASK_STACKSIZE];
OS_STK    rf_task_stk[TASK_STACKSIZE];
OS_STK    magnetic_task_stk[TASK_STACKSIZE];
OS_STK    logic_task_stk[TASK_STACKSIZE];
OS_STK	  astar_task_stk[TASK_STACKSIZE];
OS_STK    interface_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */


#define ADCTASK_PRIORITY      	1
#define LIGHTTASK_PRIORITY      5
#define RFTASK_PRIORITY      	3
#define MAGNETICTASK_PRIORITY   2
#define LOGICTASK_PRIORITY      4
#define ASTARTASK_PRIORITY		6
#define INTERFACE_PRIORITY      7


/* Definition of Global Vairables */




/* The main function creates task and starts them */
int main(void)
{
	// Initialise globals
	rfDataIndex = -1;
	// Task creation
	OSTaskCreateExt(adc_task,
			NULL,
			(void *)&adc_task_stk[TASK_STACKSIZE-1],
			ADCTASK_PRIORITY,
			ADCTASK_PRIORITY,
			adc_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSTaskCreateExt(light_task,
			NULL,
			(void *)&light_task_stk[TASK_STACKSIZE-1],
			LIGHTTASK_PRIORITY,
			LIGHTTASK_PRIORITY,
			light_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSTaskCreateExt(rf_task,
			NULL,
			(void *)&rf_task_stk[TASK_STACKSIZE-1],
			RFTASK_PRIORITY,
			RFTASK_PRIORITY,
			rf_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);

/*
	OSTaskCreateExt(magnetic_task,
			NULL,
			(void *)&magnetic_task_stk[TASK_STACKSIZE-1],
			MAGNETICTASK_PRIORITY,
			MAGNETICTASK_PRIORITY,
			magnetic_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);
*/

	OSTaskCreateExt(logic_task,
			NULL,
			(void *)&logic_task_stk[TASK_STACKSIZE-1],
			LOGICTASK_PRIORITY,
			LOGICTASK_PRIORITY,
			logic_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSTaskCreateExt(astar_task,
			NULL,
			(void *)&astar_task_stk[TASK_STACKSIZE-1],
			ASTARTASK_PRIORITY,
			ASTARTASK_PRIORITY,
			astar_task_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSTaskCreateExt(interface,
			NULL,
			(void *)&interface_stk[TASK_STACKSIZE-1],
			INTERFACE_PRIORITY,
			INTERFACE_PRIORITY,
			interface_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	//Begin the operating system
	OSStart();
	return 0;
}
