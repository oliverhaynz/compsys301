/*
 * main.h
 *
 *  Created on: 22/09/2015
 *      Author: Oliver Hay
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdio.h>
#include "includes.h"
#include "system.h"
#include "tasks/adc_task.h"
#include "tasks/rf_task.h"
#include "tasks/interface.h"
#include "tasks/light_task.h"
#include "tasks/logic_task.h"
#include "tasks/magnetic_task.h"
#include "tasks/astar_task.h"
#include "adc_main.h"
#include <alt_types.h>

#endif /* MAIN_H_ */
