#include "data.h"
#include "astar.h"

#define WALL ' '
#define PELLET '0'

unsigned int ghost_matrix[5][5] = {
		{10,10,10,10,10},
		{10,30,30,30,10},
		{10,30,50,30,10},
		{10,30,30,30,10},
		{10,10,10,10,10}};


void mark_food(void);
unsigned int get_valid(unsigned int y, unsigned int x);
unsigned int get_type(unsigned int valid);
void print_map(unsigned int x, unsigned int y);

int astar(Reduced_Path *head, unsigned int goal[2]) {
    // Variable declarations
    unsigned int initial = 1;
    unsigned int dir;
    unsigned int temp_dir;
    unsigned int x, x1;
    unsigned int y, y1;
    unsigned int i;
    unsigned int pathReady = 0;

    // Check for preprocessing
    if (!IsPreprocessed)
        init_map();
    // Check start and goal positions are not a wall
    printf("Head node [%d,%d] is type %d\n", head->node->x, head->node->y, head->node->type);
    if (!head->node->type || (goal && !node_map[goal[1]][goal[0]].type))
        return 4;
    /*
    for (y = 0; y < YSIZE; y++) {
        for (x = 0; x < XSIZE; x++) {
            if (node_map[y][x].hasFood) {
                printf("Food smelt @[%d,%d]\n", x, y);
            }
        }
    }
    */
    // Initialise goal_node
    struct node_struct *goal_node;
    if (goal) {
    	//printf("Goal node [%d,%d] is type %d\n", goal[0], goal[1], node_map[goal[1]][goal[0]].type);
        // If there is a goal store a pointer to it for comparison
        goal_node = &node_map[goal[1]][goal[0]];
    } else {
        //printf("%d food left\n", num_food);
        // Otherwise check we are not out of food
        if (!num_food) {
            // If we are out of food, return 7
            //printf("Feed me\n");
            return 7;
        }
        // Then set the goal to be NULL if we are still running
        goal_node = NULL;
    }

    //print_map(XSIZE, YSIZE);
    /*     ---     Actual A*     ---     */
    // Point the tail to the head
    Reduced_Path *tail = head;
    // Initialise Open_Path
    Open_Path *open = NULL;
    // Create temp_path variable for pointer rearranging
    Reduced_Path *temp_path = NULL;
    // Traverse map until reaching goal, setting backward information
    while (!pathReady) {
        //printf("Checking %d\n", open);
        // Follow paths
        //printf("Directions %d, %d\n", tail->node->valid, tail->backward_dir);
        dir = tail->node->valid - tail->backward_dir;
        x = tail->node->x;
        y = tail->node->y;
        //printf("Searching [%d,%d] with %d\n", x, y, dir);
        if (!tail->reachedDest && tail->node->visited) {
            // Check for revisiting
            //printf("Node visited, deleting path\n");
            free(tail);
            tail = NULL;
        } else if (initial || tail->node->type != 1) { // Not a path
			if (tail->reachedDest) {
				// If the destination has been reached then the path is ready for returning
                //printf("Hit the end\n");
				pathReady = 1;
				continue;
        	} else if (!initial && tail->node->type == 3) { // Deadend
        		// Otherwise check for a deadend and free it from memory if this is not the initial
				free(tail);
				tail = NULL;
				//printf("Hit deadend @[%d,%d]\n", x, y);
			} else { // Junction
				//printf("Hit junction @[%d,%d]\n", x, y);
				tail->node->visited = 1;
				if (initial) {
					// Ignore backward dir for checking valid directions
					dir = tail->node->valid;
				}
				// Check each direction for valid paths
				for (i = 0; i < 4; i++) {
					temp_dir = (dir & 1<<i);
					if (!temp_dir) {
						// Skip iteration if the direction is not valid
						continue;
					}
					temp_path = (Reduced_Path*)malloc(sizeof(Reduced_Path));
					x1 = x;
					y1 = y;
					switch (temp_dir) { // NSEW
						case 1<<3:     y1--; // North
									break;
						case 1<<2:     y1++; // South
									break;
						case 1<<1:     x1++; // East
									break;
						case 1<<0:     x1--; // West
									break;
						default:    break;
					}
					//printf("Checking node [%d,%d] with %d/%d and %d\n", x1, y1, temp_dir, invert_dir(temp_dir), node_map[y1][x1].valid);
					// Point to the correct node
					temp_path->node = &node_map[y1][x1];
					temp_path->forward = NULL;
					// Point backwards to the junction
					temp_path->backward = tail;
					// Init directions
					temp_path->forward_dir = 0;
					temp_path->backward_dir = invert_dir(temp_dir);
					temp_path->previous_dir = temp_dir;
					// Init lengths
					temp_path->forward_len = 0;
					temp_path->backward_len = 1;
					// Init costs, if a turn is required, add an additional cost
					temp_path->current_cost = tail->current_cost + (temp_path->backward_dir != tail->backward_dir ? 1 : 0) + (!tail->reachedDest ? 1 : 0) + ghost_costs[y1][x1];
					temp_path->estimated_cost = goal ? estimate_cost(x1, y1, goal[0], goal[1]) : 1;
					temp_path->reachedDest = tail->reachedDest;
					// Add temp_path to the open list
					open = add_open_path(open, temp_path);
					//Make sure we can't accidentally override the temp_path
					temp_path = NULL;
				}
				// Mark initial as false
				initial = 0;
				//printf("Finished junction\n");
			}
        } else { // Path
            //printf("Hit path @[%d,%d]\n", x, y);
            // Check the previous direction, move in the other one
            switch (dir) { // NSEW
                case 1<<3:     y--; // North
                            break;
                case 1<<2:     y++; // South
                            break;
                case 1<<1:     x++; // East
                            break;
                case 1<<0:     x--; // West
                            break;
                default:   //printf("Something broke @[%d,%d] with %d\n", x, y, dir);
                			return 6;
                			break;
            }
            // Point to the next node
            tail->node = &node_map[y][x];
            // Increment path counters
            tail->backward_len++;
            tail->current_cost += (!tail->reachedDest ? 1 : 0) + ghost_costs[y][x];
            // Set backward dir
            tail->backward_dir = invert_dir(dir);
            // Re-calculate cost, if there is no goal cost is always 1
            tail->estimated_cost = goal ? estimate_cost(x, y, goal[0], goal[1]) : 1;
            // Re-add the path to the open list
            open = add_open_path(open, tail);
            // Make sure we can't access the tail accidentally
            tail = NULL;
        }
        // Check there still exists an Open_Path
        if (!open->path) // Destination unreachable
            return 3; // Almost certainly leaks memory...
        // Set the tail to the best path
        tail = open->path;
        // Pop the best path off the list
        open = pop_open_path(open);
        // Check for food if goal is null
        if (!goal && !tail->reachedDest && tail->node->hasFood) {
        	//printf("Null goal, food found @[%d,%d]\n", tail->node->x, tail->node->y);
        	tail->reachedDest = 1;
            tail->current_cost++;
        // Otherwise check if the tail node is equivalent to the goal
        } else if (goal && (tail->node == goal_node)) {
            //printf("Goal reached\n");
            tail->reachedDest = 1;
        }
        //printf("%d%c\n", OSCPUUsage, '%');
        OSTimeDlyHMSM(0, 0, 0, 1);
    }
    if (!goal)
        goal_node = tail->node;
    //printf("Found path costing %d! Recursing\n", tail->current_cost);
    printf("[%d,%d]\n", goal_node->x, goal_node->y);
    // Recurse up backward path, setting forward information
    while (tail->node != head->node) {
        temp_path = tail->backward;
        //printf("Moving to node [%d,%d] in %d steps\n", temp_path->node->x, temp_path->node->y, tail->backward_len);
        temp_path->forward = tail;
        temp_path->forward_dir = tail->previous_dir;
        temp_path->forward_len = tail->backward_len;
        // For drawing the map
        switch (tail->previous_dir) {
            case 1<<3:  dir = 18; // North
                        break;
            case 1<<2:  dir = 19; // South
                        break;
            case 1<<1:  dir = 20; // East
                        break;
            case 1<<0:  dir = 21; // West
                        break;
            default:    break;
        }
        map[temp_path->node->y][temp_path->node->x] = dir;
        // Decrement tail node
        tail = temp_path;
    }
    OSTimeDlyHMSM(0,0,0,1);
    // Mark the map as unvisited
    for (y = 0; y < YSIZE; y++)
    	for (x = 0; x < XSIZE; x++)
    		node_map[y][x].visited = 0;
    // Free the open set of paths from memory
    while (open) {
        //printf("Freeing node [%d,%d]\n", open->path->node->x, open->path->node->y);
        free(open->path);
        open->path = NULL;
        open = pop_open_path(open);
    }
    /*     ---       A* End      ---     */
    //print_map(goal_node->x, goal_node->y);
    return 0;
}

// Add the path above the first Open_Path with equal or greater cost and return the new head
Open_Path *add_open_path(Open_Path *open, Reduced_Path *path) {
    //printf("\tAdding [%d,%d] @%d cost\t", path->node->x, path->node->y, path->current_cost+path->estimated_cost);
	//printf("Adding [%d,%d] with %d\n", path->node->x, path->node->y, path->backward_dir);
    Open_Path *open_check = open;
    Open_Path *next_check;
    unsigned int total_cost;
    unsigned int next_cost;
    total_cost = path->current_cost + path->estimated_cost;
    int IsDone = 0;
    // Check for empty list
    if (!open) {
        open = (Open_Path*)malloc(sizeof(Open_Path));
        open->next = NULL;
        open->path = path;
        //printf("to head (%d)\n", open);
        //printf("%d\n", open->path->backward_dir);
        return open;
    }
    // Check first node
    next_cost = open->path->current_cost + open->path->estimated_cost;
    if (total_cost <= next_cost) {
        open_check = (Open_Path*)malloc(sizeof(Open_Path));
        open_check->next = open;
        open_check->path = path;
        open = open_check;
        IsDone = 1;
        //printf("to head (%d) replacing (%d)\n", open, open->next);
    }
    // Loop until done
    while (!IsDone) {
        // If there is no next then add the path to the end
        if (!open_check->next) {
            open_check->next = (Open_Path*)malloc(sizeof(Open_Path));
            open_check->next->next = NULL;
            open_check->next->path = path;
            IsDone = 1;
            //printf("to tail (%d)\n", open_check->next);
        } else {
            next_check = open_check->next;
            // Calculate the cost of the next Open_Path
            next_cost = next_check->path->current_cost + next_check->path->estimated_cost;
            //printf("Comparing %d <= %d\n", total_cost, next_cost);
            if (total_cost <= next_cost) {
                next_check = (Open_Path*)malloc(sizeof(Open_Path));
                next_check->next = open_check->next;
                open_check->next = next_check;
                next_check->path = path;
                IsDone = 1;
                //printf("in the middle (%d)\n", next_check);
            } else {
                open_check = next_check;
            }
        }
    }
    //printf("%d, last: %d\n", open_check->next->path->backward_dir, open->path->backward_dir);
    return open;
}

// Pop the top path off the open set and return the new head
Open_Path *pop_open_path(Open_Path *open) {
    Open_Path *new;
    new = open->next;
    //printf("Popping %d, returning %d\n", open, new);
    // Release the memory
    free(open);
    open = NULL;
    //printf("%d\n", new->path->backward_dir);
    return new;
}

void mark_food(void) {
    int i;
    // Mark positions of food pellets
    for (i = 0; i < NUMFOOD; i++) {
        // food_list[i] provides {x,y} indices for pellets
        node_map[food_list[i][1]][food_list[i][0]].hasFood = 2;
        //printf("Marking food %d\n", i);
        // copy food_list to better_food_list
        better_food_list[i][0] = food_list[i][0];
        better_food_list[i][1] = food_list[i][1];
        // Increment the food count if it isn't already counted
        if (node_map[food_list[i][1]][food_list[i][0]].type == 2)
            num_food++;
    }
}

void init_map() {
    int i, j;
    unsigned int valid;
    unsigned int type;
    unsigned int num_paths = 0;
    unsigned int num_juncts = 0;
    unsigned int num_deadends = 0;
    // Initialise the food counter
    num_food = 0;
    // Loop through the whole map
    for (i = 0; i < YSIZE; i++) {
        for (j = 0; j < XSIZE; j++) {
            valid = get_valid(i, j);
            // For drawing
            map[i][j] = valid + 1;
            // Skip walls to save time
            if (valid) {
                type = get_type(valid);
                // Count each type
                if (type == 1)
                    num_paths++;
                else if (type == 2)
                    num_juncts++;
                else if (type == 3)
                    num_deadends++;
                // Fill in node_map
                node_map[i][j].x = j;
                node_map[i][j].y = i;
                node_map[i][j].type = type;
                node_map[i][j].valid = valid;
                node_map[i][j].visited = 0;
                if (type != 2) {
                    node_map[i][j].hasFood = 1;
                    num_food++;
                } else {
                    node_map[i][j].hasFood = 0;
                }
                hit_count[i][j] = 0;
            } else
                hit_count[i][j] = -1;
            ghost_costs[i][j] = 0;
        }
    }
    mark_food();
	IsPreprocessed = 1;
    //printf("%s:%3d, %s:%3d, %s:%d, %s:%d\n", "Paths", num_paths, "Junctions", num_juncts, "Deadends", num_deadends, "Food", num_food);
}

unsigned int convert_rf_pos(unsigned int val, int IsX) {
    unsigned int result;
    if (IsX)
        result = (val - XOFFSET + XMULT/2 - 1)/XMULT;
    else
        result = (val - YOFFSET + YMULT/2 - 1)/YMULT;
    return result;
}

void mark_ghosts(int numGhosts, int *ghostPos) {
    int i;
    int j,k;
    int x, y;
    int ghostX;
    int ghostY;
    //printf("Number of ghosts: %d\n", numGhosts);

    // Initialise ghost_cost map to 0
    for (i = 0; i < YSIZE; i++) {
        for (j = 0; j < XSIZE; j++) {
            ghost_costs[i][j] = 0;
        }
    }
    // Iterate over each ghost
    for (i = 0; i < numGhosts; i++) {
        // Get the x and y positions then convert to map coordinates
        ghostX = ghostPos[i*2 + 0];
        ghostX -= XOFFSET;
        ghostX = (ghostX + XMULT/2 - 1)/XMULT;
        ghostY = ghostPos[i*2 + 1];
        ghostY -= YOFFSET;
		ghostY = (ghostY + YMULT/2 - 1)/YMULT;
        //printf("ghost %d [%d, %d]\n", i,  ghostX, ghostY);

        for(j = 0; j < 5; j++){
        	OSTimeDlyHMSM(0, 0, 0, 1);
        	y = ghostY - 2 + j;
        	for(k = 0; k < 5; k++){
        		x = ghostX - 2 + k;
        		if ((x > 0 && x < XSIZE) && (y > 0 && y < YSIZE))
        			ghost_costs[y][x] = ghost_matrix[j][k];
        	}
        }

    }

}

unsigned int invert_dir(unsigned int dir) {
    switch (dir) { // NSEW
        case 1<<3:     return 1<<2; // South
        case 1<<2:     return 1<<3; // North
        case 1<<1:     return 1<<0; // West
        case 1<<0:     return 1<<1; // East
        default:    return 0;     // ?
    }
}

unsigned int convert_rf_dir(unsigned int angle) {
	if (angle > 600 && angle < 1200) { // inverted directions
		return 0b0100; // NORTH
	} else if (angle > 2400 && angle < 3000) {
		return 0b1000; // SOUTH
	} else if (angle > 3300 || angle < 300) {
		return 0b0001; // EAST
	} else if (angle > 1500 && angle < 2100) {
		return 0b0010; // WEST
	} else return 0b0000; // Not aligned
}

unsigned int estimate_cost(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2) {
    unsigned int x;
    unsigned int y;
    x = x1 > x2 ? x1 - x2: x2 - x1;
    y = y1 > y2 ? y1 - y2: y2 - y1;
    return x + y;
}

void mark_eaten(Reduced_Path *current) {
	// Take the current path and follow it to the next node, marking all nodes in between as having been eaten
	// Will always be a straight line so we can (ab)use that
	Reduced_Path *next = current->forward;
	struct node_struct *node = current->node;
    unsigned int x = node->x;
    unsigned int y = node->y;
    while (node != next->node) {
    	// Increment in the forward direction
    	switch (current->forward_dir) { // NSEW
    		case 1<<3:     y--; // North
    					break;
    		case 1<<2:     y++; // South
    					break;
    		case 1<<1:     x++; // East
    					break;
    		case 1<<0:     x--; // West
    					break;
    		default:    break;
    	}
    	// Point to the next node
    	node = &node_map[y][x];
        hit_count[y][x]++;
    	// Mark the node has having its food being eaten
        if (node->hasFood) {
            node->hasFood = 0;
            num_food--;
            //printf("Marked [%d,%d] as eaten\n", x, y);
        }
    }
}

unsigned int get_valid(unsigned int y, unsigned int x) {
    // Check for wall
    if (map[y][x] == 1)
        return 0;
    // NSEW
    unsigned int valid = 0;
    if (y > 0 && map[y-1][x] != 1)
        valid ^= 1<<3;
    if (y < YSIZE && map[y+1][x] != 1)
        valid ^= 1<<2;
    if (x < XSIZE && map[y][x+1] != 1)
        valid ^= 1<<1;
    if (x > 0 && map[y][x-1] != 1)
        valid ^= 1<<0;
    return valid;
}

unsigned int get_type(unsigned int valid) {
    // only check 4 bits of lowest significance
    valid &= 0b1111;
    if (valid == 0b0000)
        return 0; // wall
    if (valid == 0b0001 || valid == 0b0010 || valid == 0b0100 || valid == 0b1000)
        return 3; // deadend
    if (valid == 0b1100 || valid == 0b0011)
        return 1; // path
    // everything else is junction
    return 2;
}

void print_map(unsigned int x, unsigned int y) {
    unsigned int i, j;
    char disp;
    for (i = 0; i < YSIZE; i++) {
        for (j = 0; j < XSIZE; j++) {
            if (y == i && x == j) {
                printf("X");
                continue;
            }
            switch(map[i][j]) {
                case 0:     disp = PELLET;
                            break;
                case 1:     disp = WALL;
                            break;
                case 2:     disp = 180;
                            break;
                case 3:     disp = 195;
                            break;
                case 4:     disp = 205;
                            break;
                case 5:     disp = 194;
                            break;
                case 6:     disp = 187;
                            break;
                case 7:     disp = 201;
                            break;
                case 8:     disp = 203;
                            break;
                case 9:     disp = 193;
                            break;
                case 10:    disp = 188;
                            break;
                case 11:    disp = 200;
                            break;
                case 12:    disp = 202;
                            break;
                case 13:    disp = 186;
                            break;
                case 14:    disp = 185;
                            break;
                case 15:    disp = 204;
                            break;
                case 16:    disp = 206;
                            break;
                case 17:    disp = 249;
                            break;
                case 18:    disp = '^';
                            break;
                case 19:    disp = '.'; // No down arrow :(
                            break;
                case 20:    disp = '>';
                            break;
                case 21:    disp = '<';
                            break;
                default:    disp = map[i][j] + '@';
                            break;
            }
            printf("%c", disp);
        }
        printf("\n");
    }
}
