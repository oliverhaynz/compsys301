/*
 * astar.h
 *
 *  Created on: 30/09/2015
 *      Author: Tyrone
 */

#ifndef ASTAR_H_
#define ASTAR_H_
// Constant definitions
#define YSIZE 15
#define XSIZE 19
#define NUMFOOD 0
#define XOFFSET 24
#define XMULT 53
#define YOFFSET 23
#define YMULT 51

unsigned int better_food_list[NUMFOOD][2];
int hit_count[YSIZE][XSIZE];

// Includes
#include "includes.h"
#include <stdio.h>
#include <stdlib.h>

// Global variables
int IsPreprocessed;
int num_food;

unsigned int ghost_costs[YSIZE][XSIZE];

// Struct definitions
struct node_struct {
    // 5 bit x & y
    unsigned int x:5;
    unsigned int y:5;
    // 2 bit type: wall, path, junction, dead end
    unsigned int type:2;
    // 4 bit valid direction: NSEW
    unsigned int valid:4;
	// 1 bit visitation check for pathfinding
	unsigned int visited:1;
	// 2 bit food check, 1 for general food, 2 for potential super food
	unsigned int hasFood:2;
    // extra 13 bits here
} node_map[YSIZE][XSIZE]; // 4Byte struct

typedef struct reduced_path_struct { // for returning path w/junctions
    struct node_struct *node;
    // doubly linked list
    struct reduced_path_struct *forward; // NULL for TAIL
    struct reduced_path_struct *backward; // NULL for HEAD
    // direction in and out
    unsigned int forward_dir:4; // 0 for TAIL
    unsigned int backward_dir:4; // 0 for HEAD
    unsigned int previous_dir:4; // for setting the previous forward_dir
    // path length
    unsigned int forward_len:8;
    unsigned int backward_len:8;
    // cost fields
    unsigned int current_cost:16;
    unsigned int estimated_cost:16;
    // check for destination reached
    unsigned int reachedDest:1;
    // 7 bits extra
} Reduced_Path; // 20Byte struct

typedef struct open_list_struct { // for storing open paths
    // Should be sorted based on the costs in the path
    Reduced_Path *path;
    // singly linked list
    struct open_list_struct *next;
} Open_Path;

Open_Path *add_open_path(Open_Path *open, Reduced_Path *path);
Open_Path *pop_open_path(Open_Path *open);

// Function declarations
// {x,y} inputs
int astar(Reduced_Path *head, unsigned int goal[2]);
unsigned int estimate_cost(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
void mark_eaten(Reduced_Path *current);
unsigned int invert_dir(unsigned int dir);
unsigned int convert_rf_dir(unsigned int angle);
void init_map();
unsigned int convert_rf_pos(unsigned int val, int IsX);
void mark_ghosts(int numGhosts, int *ghostPos);


#endif /* ASTAR_H_ */
