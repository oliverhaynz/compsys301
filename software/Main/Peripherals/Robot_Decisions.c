/*
 * Robot_Decisions.c
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

/*Checks transistor status and returns a decision based on where the robot should go in each situation
 * INPUTS: 	gpio - The status of the transistors from 0b00000 (all off) to 0b11111 (all on)
 *			mode - Whether the device should take a command from the gpio pins or the cmd.h file
 * OUTPUTS: instr- Instruction for the robot.
 * */
act_on_transistor_status(int gpio, int mode){

	int instr = mode;

	if(mode == 5){
		switch(gpio){
		case 0b00000://All lights off... Break
			instr = 4;
			break;
		case 0b11111://All lights off... Break
			instr = 0;
			break;
		case 0b01111://All lights off... Break
			instr = 0;
			break;


			//Front sensor on
		case 0b11010:
			instr = 2;
			break;
		case 0b11110://Right on only
			instr = 2;
			break;
		case 0b10010:
			instr = 2;
			break;
		case 0b11101://Inner Right on only
			instr = 20;
			break;
		case 0b11100:
			instr = 2;
			break;
		case 0b11000:
			instr = 2;
			break;

			//FS off
		case 0b01110://Right on only
			instr = 2;
			break;
		case 0b00010:
			instr = 2;
			break;
		case 0b01101://Inner Right on only
			//									instr = 20;
			instr = 22;
			break;
		case 0b01100:
			instr = 2;
			break;

			//Front sensor on
		case 0b10101:
			instr = 1;
			break;
		case 0b10111://Left on only
			instr = 1;
			break;
		case 0b10001://Left on only
			instr = 1;
			break;
		case 0b11011://Inner Right on only
			instr = 10;
			break;
		case 0b10011:
			instr = 1;
			break;

			//Front sensor off
		case 0b00111://Left on only
			instr = 1;
			break;
		case 0b00001://Left on only
			instr = 1;
			break;
		case 0b01011://Inner Right on only
			//instr = 10;
			instr = 11;
			break;
		case 0b00011:
			instr = 1;
			break;



		default:
			instr  = 0;
			break;
		}
	}


}


