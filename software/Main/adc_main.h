/*
 * adc_main.h
 *
 *  Created on: 1/09/2015
 *      Author: danie
 */

#ifndef ADC_MAIN_H_
#define ADC_MAIN_H_

int adc_read(int device);

void adc_init();


#endif /* ADC_MAIN_H_ */
