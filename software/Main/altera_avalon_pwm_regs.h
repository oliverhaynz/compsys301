

#ifndef __ALTERA_AVALON_PWM_REGS_H__
#define __ALTERA_AVALON_PWM_REGS_H__

#include <io.h>

#define IORD_ALTERA_AVALON_PWM_CLOCK_DIVIDER(base)       IORD(base, 0)
#define IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(base, data) IOWR(base, 0, data)
#define ALTERA_AVALON_PWM_CLOCK_DIVIDER_MSK              (0xFFFFFFFF)
#define ALTERA_AVALON_PWM_CLOCK_DIVIDER_OFST             (0)

#define IORD_ALTERA_AVALON_PWM_DUTY_CYCLE(base)         IORD(base, 1)
#define IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(base, data)   IOWR(base, 1, data)
#define ALTERA_AVALON_PWM_DUTY_CYCLE_MSK                (0xFFFFFFFF)
#define ALTERA_AVALON_PWM_DUTY_CYCLE_OFST               (0)

#define IORD_ALTERA_AVALON_PWM_ENABLE(base)             IORD(base, 2)
#define IOWR_ALTERA_AVALON_PWM_ENABLE(base, data)       IOWR(base, 2, data)
#define ALTERA_AVALON_PWM_ENABLE_MSK                    (0x1)
#define ALTERA_AVALON_PWM_ENABLE_OFST                   (0)

//#define IORD_ALTERA_AVALON_PWM_ENABLE(base)             IORD(base, 2)
//#define IOWR_ALTERA_AVALON_PWM_ENABLE(base, data)       IOWR(base, 2, data)
//#define ALTERA_AVALON_PWM_ENABLE_MSK                    (0x1)
//#define ALTERA_AVALON_PWM_ENABLE_OFST                   (0)

#define IORD_ALTERA_AVALON_PWM_MODE(base)               IORD(base, 3)
#define IOWR_ALTERA_AVALON_PWM_MODE(base, data)         IOWR(base, 3, data)
#define ALTERA_AVALON_PWM_MODE_MSK                      (0x1)
#define ALTERA_AVALON_PWM_MODE_OFST                     (0)


#endif /* __ALTERA_AVALON_PWM_REGS_H__ */
