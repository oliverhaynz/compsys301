/*
 * logic_task.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef LOGIC_TASK_H_
#define LOGIC_TASK_H_

#include <stdio.h>
#include <stdlib.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "../altera_avalon_pwm_regs.h"
#include "../altera_avalon_pwm_routines.h"
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
// Task includes
#include "rf_task.h"
#include "magnetic_task.h"
#include "../Peripherals/astar.h"
#include "astar_task.h"
// PD constants
#define KP 0.08
#define KD 0.04
#define Kp_speed 60
#define Ki_speed 1.2
#define Kd_speed 0.1
// Speed modifiers
#define GLOBAL_RIGHT 1
#define GLOBAL_LEFT 0.92
#define MAX_PWM 1000 //Protection For broken motor TODO;
#define DIST_LIMIT 1780


void logic_task(void* pdata);
void InitilisePWM(void);
int SpeedControl(void);
void motor_movements(int left, int right);
void FollowLine();
void ExecuteTurn(int direction);
int CheckTurn();
int CheckRFIntersection();

#define PX_DIST 25
int getDirection();


extern alt_u8 lightSensor[5];
extern int lightSensorIndex;
extern int rfDataIndex;
int Max_Speed;
int DesiredSpeed;
#endif /* LOGIC_TASK_H_ */
