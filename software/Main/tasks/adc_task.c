/*
 * adc_task.c
 *
 *  Created on: 18/09/2015
 *      Author: Daniel
 */
#include "adc_task.h"

void adc_task(void* pdata){
	//Looping variable
	int channel = 1;
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 10);
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 10);

	while(1){
		//Read Each 3 times.
		channels.inner_left = adc_read(0);
		channels.inner_left = adc_read(0);
		channels.inner_left = adc_read(0);
		channels.inner_right= adc_read(1);
		channels.inner_right= adc_read(1);
		channels.inner_right= adc_read(1);
		channels.Wheel1 	= adc_read(2);
		channels.Wheel1 	= adc_read(2);
		channels.Wheel1 	= adc_read(2);
		channels.Wheel2 	= adc_read(3);
		channels.Wheel2 	= adc_read(3);
		channels.Wheel2 	= adc_read(3);
		channels.Voltage 	= adc_read(4);
		channels.Voltage 	= adc_read(4);
		channels.Voltage 	= adc_read(4);
		channels.far_right 	= adc_read(5);
		channels.far_right	= adc_read(5);
		channels.far_right	= adc_read(5);
		channels.far_left 	= adc_read(6);
		channels.far_left 	= adc_read(6);
		channels.far_left 	= adc_read(6);
		channels.front 		= adc_read(7);
		channels.front 		= adc_read(7);
		channels.front	 	= adc_read(7);
		OSTimeDlyHMSM(0,0,0,10);
//		printf("%d\t%d\t%d\t%d\t%d\n",  channels.inner_left, channels.inner_right, channels.far_left, channels.far_right, channels.front);
	}
	return;
}
