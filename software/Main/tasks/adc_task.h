/*
 * adc_task.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef ADC_TASK_H_
#define ADC_TASK_H_

#include <stdio.h>
#include "includes.h"
#include "system.h"

#define ADC_CHANNELS 8

struct Channels{
	int Wheel1;
	int inner_right;
	int inner_left;
	int Wheel2;
	int Voltage;
	int far_left;
	int far_right;
	int front;
} channels;

void adc_task(void* pdata);


#endif /* ADC_TASK_H_ */
