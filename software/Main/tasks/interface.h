/*
 * interface.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_

#include <stdio.h>
#include "includes.h"
#include "system.h"
// Task includes
#include "logic_task.h"
#define INTERFACE_MODE 6

void interface(void* pdata);

#endif /* INTERFACE_H_ */
