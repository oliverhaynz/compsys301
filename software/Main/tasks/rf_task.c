/*
 * rf_task.c
 *
 *  Created on: 18/09/2015
 *      Author: Daniel
 */
#include "rf_task.h"

int huns[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void rf_task(void* pdata){
	rfDataIndex = 0;

	FILE* fp;
	int i = 6;
	int j;
	int val;
	int tens[6] = {1, 10, 100, 1000, 10000, 100000};
	char prompt;
	int k=0;
	int open_count = 0, close_count = 0, comma_count = 0;



	char buffer[6];
	//	  printf("Opening UART");
	fp = fopen("/dev/uart0", "r");
	while(1){
		prompt = getc(fp);
		//printf("%c",prompt);
		if(prompt == ',' || prompt == '\n'){
			// convert buffer and store the value
			val = 0;
			for (j = 0; (i + j) < 6; j++) {
				// convert and store
				val += (int)buffer[i + j] * tens[j];
			}
			i = 6;
			// print
			huns[k] = val;
//			printf("%d,", huns[k]);
			if(++k > 17){
				k = 17;
			}
			if (prompt == '\n' && huns[1] < 15 && huns[1] > 0) {
				// Check the number of brackets and compare with k
				if (open_count == close_count && comma_count == (close_count*4 + 1)) {
					ToStruct();
					//printf("%s %d,%d,[%d,%d,%d]\n", "Address:", huns[0], huns[1], huns[2], huns[3], huns[4]);
//					printf("%d\n", huns[4]);
					OSTimeDlyHMSM(0, 0, 0, 50);
				} else {
					//printf("Dropped RF: [:%d ]:%d ,:%d\n", open_count, close_count, comma_count);
				}
			} else if (prompt == ',') {
				comma_count++;
			}

		} else if (prompt >= '0' && prompt <= '9') {
			// decrement i
			i--;
			// load prompt into buffer
			buffer[i] = prompt - '0';
		} else if(prompt == '#'){
//						printf("\n");
			// reset i
			i = 6;
			k = 0;
			open_count = 0;
			close_count = 0;
			comma_count = 0;
		} else if(prompt == '-'){
			//			printf("-");
		} else if(prompt == '[') {
			open_count++;
		} else if(prompt == ']') {
			close_count++;
		}

		OSTimeDlyHMSM(0, 0, 0, 1);
	}
	fclose(fp);
	return;
}

void ToStruct(void){
	if(++rfDataIndex >= 5)
			rfDataIndex = 0;

	rfDataHist[rfDataIndex].Index = huns[0];
	rfDataHist[rfDataIndex].Address = huns[1];
	rfDataHist[rfDataIndex].Robot_X = huns[2];
	rfDataHist[rfDataIndex].Robot_Y = huns[3];
	rfDataHist[rfDataIndex].Robot_A = huns[4];
	rfDataHist[rfDataIndex].Ghost0_X = huns[5];
	rfDataHist[rfDataIndex].Ghost0_Y = huns[6];
	rfDataHist[rfDataIndex].Ghost0_Speed = huns[7];
	rfDataHist[rfDataIndex].Ghost0_A = huns[8];
	rfDataHist[rfDataIndex].Ghost1_X = huns[9];
	rfDataHist[rfDataIndex].Ghost1_Y = huns[10];
	rfDataHist[rfDataIndex].Ghost1_Speed = huns[11];
	rfDataHist[rfDataIndex].Ghost1_A = huns[12];
	rfDataHist[rfDataIndex].Ghost2_X = huns[13];
	rfDataHist[rfDataIndex].Ghost2_Y = huns[14];
	rfDataHist[rfDataIndex].Ghost2_Speed = huns[15];
	rfDataHist[rfDataIndex].Ghost2_A = huns[16];
	rfDataHist[rfDataIndex].RSSI = huns[17];
	rfDataHist[rfDataIndex].Time = clock();
	return;
}
