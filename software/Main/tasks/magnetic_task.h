/*
 * magnetic_task.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef MAGNETIC_TASK_H_
#define MAGNETIC_TASK_H_

#include <stdio.h>
#include "includes.h"
#include "system.h"
#include <time.h>
// Task includes
#include "adc_task.h"

#define WHEEL_HIGH 2000
#define WHEEL_LOW 1800

void magnetic_task(void* pdata);

struct Robot_Speed{
	int wheel1_counter;
	int wheel2_counter;
	int instantanious;
	int average;
	int distance;
} robot_speed;

#endif /* MAGNETIC_TASK_H_ */
