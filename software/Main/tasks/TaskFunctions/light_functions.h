/*
 * light_functions.h
 *
 *  Created on: 15/10/2015
 *      Author: danie
 */

#ifndef LIGHT_FUNCTIONS_H_
#define LIGHT_FUNCTIONS_H_

#define NUM_AVG 6
#include "../rf_task.h"

int checkThreshold(int x_threshold,int upper, int lower);
int getDifference(int new, int old);
int average_reading(int input[NUM_AVG]);
int cvt_bin2int(int bin[5]);

#endif /* LIGHT_FUNCTIONS_H_ */
