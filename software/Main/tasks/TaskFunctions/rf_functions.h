/*
 * rf_functions.h
 *
 *  Created on: 15/10/2015
 *      Author: danie
 */

#ifndef RF_FUNCTIONS_H_
#define RF_FUNCTIONS_H_

int store_as_integer(char buffer[6], int tens[6], int i);
int endline(char prompt, int huns[18]);
int check_valid(int open_count, int close_count, int comma_count);
int isNumeric(char prompt);
int startline(char prompt);

#endif /* RF_FUNCTIONS_H_ */
