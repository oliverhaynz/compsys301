/*
 * rf_functions.c
 *
 *  Created on: 15/10/2015
 *      Author: danie
 */
#include "rf_functions.h"


int store_as_integer(char buffer[6], int tens[6], int i){
	int val = 0;
	int j = 0;

	for (j = 0; (i + j) < 6; j++) {
		// convert and store
		val += (int)buffer[i + j] * tens[j];
	}
	return val;
}

int endline(char prompt, int huns[18]){
	return prompt == '\n' && huns[1] < 15 && huns[1] > 0;
}

int check_valid(int open_count, int close_count, int comma_count){
	return open_count == close_count && comma_count == (close_count*4 + 1);
}

int isNumeric(char prompt){
	return prompt >= '0' && prompt <= '9';
}

int startline(char prompt){
	return(prompt == '#');
}
