/*
 * light_functions.c
 *
 *  Created on: 15/10/2015
 *      Author: danie
 */
#include "light_functions.h"

int checkThreshold(int x_threshold, int upper, int lower){
	return rfDataHist[0].Robot_Y > x_threshold?lower:upper;
}

int getDifference(int new, int old){
	int check = 0;
	check = new - old;
	//Return +'ve difference always
	return check > 0 ? check:-check;
}

int average_reading(int input[NUM_AVG]){
	int i = 0;
	int avg = 0;
	for(i = 0; i < NUM_AVG; i++){
		avg += input[i];
	}
	avg /= NUM_AVG;
	return avg;
}

int cvt_bin2int(int bin[5]){
	int integer = 0;
	integer = bin[0]*10000+ bin[1]*1000+bin[2]*100+bin[3]*10+bin[4];
	return integer;
}
