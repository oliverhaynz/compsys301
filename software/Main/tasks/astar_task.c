/*
 * astar_task.c
 *
 *  Created on: 5/10/2015
 *      Author: danie
 */

#include "astar_task.h"

int startFlag;
int continueFlag;
int food_index;

unsigned int *find_goal(int foodIndex);

static int min(a, b) {return a > b ? b : a;}
static int max(a, b) {return a > b ? a : b;}

void astar_task(void* pdata) {
	// Initialise globals
	IsPreprocessed = 0;
	IsReady = 0;
	// And privates
	startFlag = 0;
	continueFlag = 0;
	food_index = 0;

	// Other declarations
	int returnval;
	unsigned int *goal = NULL;
	int numGhosts;
	int ghostPos[6];
	struct RFData rfData;

	int x, y;
	int a, b;
	int dir;
	unsigned int tempLen;
	Reduced_Path *tail;

	// initialise the map
	init_map();
	//printf("Initial goal node [%d,%d]\n", better_food_list[0][0], better_food_list[0][1]);
	//startFlag = 1; continueFlag = 1; food_index = 0;

	while(1) {
		//printf("i'm alive!\n");
		goal = find_goal(food_index);
		//printf("Setting goal to [%d,%d]\n", goal[0], goal[1]);
		// Load rfData
		rfData = rfDataHist[rfDataIndex];
		numGhosts = 0;
		// Get the position of each ghost
		if (rfData.Ghost0_Speed > 0) {
			ghostPos[numGhosts*2 + 0] = rfData.Ghost0_X;
			ghostPos[numGhosts*2 + 1] = rfData.Ghost0_Y;
			numGhosts++;
			//printf("task ghost 1: {%d, %d}\n", rfData.Ghost0_X,rfData.Ghost0_Y );
		}
		if (rfData.Ghost1_Speed > 0) {
			ghostPos[numGhosts*2 + 0] = rfData.Ghost1_X;
			ghostPos[numGhosts*2 + 1] = rfData.Ghost1_Y;
			numGhosts++;
		}
		if (rfData.Ghost2_Speed > 0) {
			ghostPos[numGhosts*2 + 0] = rfData.Ghost2_X;
			ghostPos[numGhosts*2 + 1] = rfData.Ghost2_Y;
			numGhosts++;
		}
		//printf("calling mark_ghosts\n");
		mark_ghosts(numGhosts, ghostPos);
		// Check flags
		if (startFlag) {
			//printf("Setting goal to [%d,%d]\n", goal[0], goal[1]);
			// Set start position from current position in RF
			// Convert from pixel position to map coordinates
			x = rfDataHist[rfDataIndex].Robot_X;
			x -= XOFFSET;
			x = (x + XMULT/2 - 1)/XMULT;
			y = rfDataHist[rfDataIndex].Robot_Y;
			y -= YOFFSET;
			y = (y + YMULT/2 - 1)/YMULT;
			// Set angle from RF
			dir = convert_rf_dir(rfDataHist[rfDataIndex].Robot_A);
			// Check x and y are on the map and the dir is valid
			//x = 3; y = 5; dir = 0b0001;
			if ((x <= 0 || x >= XSIZE) || (y <= 0 || y >= YSIZE) || !dir) {
				PositionError = 1;
				OSTimeDlyHMSM(0, 0, 0, 100);
				continue;
			} else {
				PositionError = 0;
				//printf("Starting position [%d,%d] from [%d,%d]\n", x, y, rfDataHist[rfDataIndex].Robot_X, rfDataHist[rfDataIndex].Robot_Y);
			}
			// Allocate memory for the path
			path = (Reduced_Path*)malloc(sizeof(Reduced_Path));
			// Point head node to start node
			path->node = &node_map[y][x];
			path->forward = NULL;
			path->backward = NULL;
			// Init directions
			path->forward_dir = 0;
			path->backward_dir = dir;
			path->previous_dir = 0;
			// Init lengths
			path->forward_len = 0;
			path->backward_len = 0;
			// Init costs
			path->current_cost = 0;
			path->estimated_cost = goal ? estimate_cost(path->node->x, path->node->y, goal[0], goal[1]) : 0;
			path->reachedDest = 0;
			// Pathfind
			returnval = astar(path, goal);
			IsReady = !returnval;
			//printf("Ready flag set from %d\n\n", returnval);
			startFlag = 0;
			// Free goal node memory
			free(goal);
			goal = NULL;
		} else if (continueFlag) {
			// Free the tail of the current path)
			tail = path->forward;
			//printf("Tail %d\n", tail);
			if (tail->forward) {
				tail = tail->forward;
				while (tail->forward) {
					tail = tail->forward;
					free(tail->backward);
				}
			}
			//printf("%d\n", tail);
			if (tail != path->forward) {
				free(tail);
				// Set the tail to the next node
				tail = path->forward;
			}
			//printf("%d\n", tail);
			if(goal){
				if (tail->node->x == goal[0] && tail->node->y == goal[1]) {
					goal = find_goal(++food_index);
				} else {
					if (path->node->x == tail->node->x) {
						// Check the y position if the x's are the same
						a = path->node->y;
						b = tail->node->y;
					} else if (path->node->y == tail->node->y) {
						// Check the x position if the y's are the same
						a = path->node->x;
						b = tail->node->x;
					}
					if (goal[1] > min(a, b) && goal[1] < max(a,b))
						goal = find_goal(++food_index);
				}
			}
			if (tail->node->type == 3)
				// Force backward_len to 0 for a deadend, this makes it act as a head node
				tempLen = 0;
			else
				tempLen = tail->backward_len;
			// Make sure some stuff is 0
			tail->forward_dir = 0;
			tail->previous_dir = 0;
			tail->forward_len = 0;
			tail->backward_len = 0;
			// Re-init costs
			tail->current_cost = 0;
			tail->estimated_cost = goal ? estimate_cost(tail->node->x, tail->node->y, goal[0], goal[1]) : 0;
			tail->reachedDest = 0;
			// Pathfind
			returnval = astar(tail, goal);
			IsReady = !returnval;
			//printf("Ready flag set from %d\n\n", returnval);
			// Restore backward length if necessary
			tail->backward_len = tempLen;
			//if (returnval == 6)
			//	continueFlag = 0;
			continueFlag = 0;
		}
		OSTimeDlyHMSM(0, 0, 0, 100);
	}
}

unsigned int *find_goal(int foodIndex) {
	unsigned int *goal = NULL;
	// Check goal and set if necessary
	if (foodIndex < NUMFOOD) {
		goal = (unsigned int*)calloc(2, sizeof(int));
		// If there is food, path to it
		goal[0] = better_food_list[foodIndex][0];
		goal[1] = better_food_list[foodIndex][1];
		//printf("Set goal to [%d,%d]\n", goal[0], goal[1]);
	}
	return goal;
}

void start_astar() {
	// sets flag for starting a new path from the current position
	startFlag = 1;
	IsReady = 0;
}

void continue_astar() {
	// sets flag for continuing the current path for the next goal
	// uses the tail node for the next head
	continueFlag = 1;
}
