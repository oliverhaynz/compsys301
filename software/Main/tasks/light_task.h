/*
 * light_task.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef LIGHT_TASK_H_
#define LIGHT_TASK_H

#include <stdio.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
// Task includes
#include "adc_task.h"

void light_task(void* pdata);
alt_u8 lightSensor[5];
int lightSensorIndex;
int LIGHT_STATUS;

#endif /* LIGHT_TASK_H_ */
