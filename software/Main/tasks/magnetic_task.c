/*
 * magnetic_task.c
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#include "magnetic_task.h"
#include "light_task.h"
#include "altera_avalon_pio_regs.h"
#include <system.h>

//Global values to hold high and low
int PRIVATE_Wheel1_high, PRIVATE_Wheel1_low,PRIVATE_Wheel2_high, PRIVATE_Wheel2_low;
double PRIVATE_prev_t1, PRIVATE_prev_t2, t1 = 0, t2 = 0;
double old_clock = 0;
int check_wheel_high(int Wheel, int currentVal);
int check_wheel_low(int Wheel, int currentVal);
int increment_wheel_counter(int checkHigh, int checkLow, double t1, double t2);
int average(int arg1, int arg2);
double calculate_average_speed(int average_wheel_turns, double t1, double t2);
double calculate_instantanious_speed(double t1,double t2);
int check_reset();

int Wheel1_old, Wheel1_diff;

void magnetic_task(void* pdata){

	int left = 0, right = 0;
	//int average_wheel_turns = 0;
	int Wheel1=0, Wheel2=0, Wheel1_Last=0, Wheel2_Last=0;
	//Initialise wheel counters
	robot_speed.wheel1_counter = 0;
	robot_speed.wheel2_counter = 0;

	//printf("\n");
	//Initialise variables for instantanious speed
	PRIVATE_prev_t1 = clock(); PRIVATE_prev_t2 = clock();
	while(1){
		//blocking
		while(check_reset()){
			OSTimeDlyHMSM(0, 0, 0, 10);
		};
		if (Wheel1) {
			// Check low
			Wheel1 = channels.Wheel1 >= WHEEL_LOW;
		} else {
			// Check high
			Wheel1 = channels.Wheel1 > WHEEL_HIGH;
		}
		if (Wheel2) {
			// Check low
			Wheel2 = channels.Wheel2 >= WHEEL_LOW;
			//printf("Checking Wheel2 low: %d %d\t", Wheel2, channels.Wheel2);
		} else {
			// Check high
			Wheel2 = channels.Wheel2 > WHEEL_HIGH;
			//printf("Checking Wheel2 high: %d %d\t", Wheel2, channels.Wheel2);
		}

		if (Wheel1 != Wheel1_Last) {
			// increment counter
			robot_speed.wheel1_counter++;
			Wheel1_Last = Wheel1;
			// update time
			if (old_clock == 0)
				old_clock = clock();
			t1 = clock()-old_clock;
		}
		//printf("Checking Wheel2 %d & %d\t", Wheel2, Wheel2_Last);
		if (Wheel2 != Wheel2_Last) {
			// increment counter
			robot_speed.wheel2_counter++;
			Wheel2_Last = Wheel2;
			// update time
			if (old_clock == 0)
				old_clock = clock();
			t2 = clock()-old_clock;
		}
		//Distance in mm
		robot_speed.distance = (robot_speed.wheel1_counter + robot_speed.wheel2_counter) * 27 / 2;
		robot_speed.average = calculate_average_speed(robot_speed.distance, t1, t2);
		//printf("%d\t%d\t%d\t%d\n", 0, robot_speed.wheel1_counter, robot_speed.wheel2_counter, robot_speed.average);
//		if (robot_speed.distance > 10)
//			//printf("%d\n", robot_speed.average);
		OSTimeDlyHMSM(0, 0, 0, 10);
	}
	return;
}

/*	returns a 1 if the wheel goes above 3000 and continues
 *	to return 1 until cleared.
 */
int check_wheel_high(int Wheel, int currentVal){

	if(Wheel > 2000 || currentVal){
		return 1;
	}else{
		return 0;
	}
}

/*	returns a 1 if the wheel goes below 1400 and continues
 *	to return 1 until cleared.
 */
int check_wheel_low(int Wheel, int currentVal){

	if(Wheel < 1800 || currentVal){
		return 1;
	}else{
		return 0;
	}
}

int increment_wheel_counter(int checkHigh, int checkLow,double t1, double t2){
	if(checkHigh && checkLow){
		return 1;
	}else{
		return 0;
	}
}

int average(int arg1, int arg2){
	return (arg1 + arg2)/2;
}

double calculate_average_speed(int average_wheel_turns, double t1, double t2){
	int speed;
	speed = (average_wheel_turns*1000)/average(t1, t2);
	if(speed > 0 && speed < 220){
		return speed;
	}else{
		return 80;
	}
}

double calculate_instantanious_speed(double t1,double t2){
	int speed;
	//	//printf("%f\t", t1-PRIVATE_prev_t1);
	speed = 55000/(t1 - PRIVATE_prev_t1);
	PRIVATE_prev_t1 = t1; PRIVATE_prev_t2 = t2;
	return speed;
}

int check_reset(){
	//	//printf("%d", lightSensor[lightSensorIndex] & 7);
	if((lightSensor[lightSensorIndex] & 3) == 0){
		robot_speed.distance = 0;
		robot_speed.average = 0;
		robot_speed.instantanious = 0;
		robot_speed.wheel1_counter = 0;
		robot_speed.wheel2_counter = 0;
		old_clock = 0;
		return 1;
	}
	return 0;
}
