/*
 * astar_task.h
 *
 *  Created on: 5/10/2015
 *      Author: danie
 */

#ifndef ASTAR_TASK_H_
#define ASTAR_TASK_H_

#include "../Peripherals/astar.h"
#include "rf_task.h"
#include "includes.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

Reduced_Path *path;
Reduced_Path *tail;

int IsReady;
int PositionError;

void astar_task(void* pdata);
void start_astar();
void continue_astar();

#endif /* ASTAR_TASK_H_ */
