/*
 * light_task.c
 *
 *  Created on: 18/09/2015
 *      Author: daniel
 */
#include "light_task.h"
#include "adc_task.h"
#include "rf_task.h"

void light_task(void* pdata){

	int read = 0;
	int last[5];
	int new[5];
//	int history[3];
	int new_check[5], old_check[5][5];
	int counter;
	int single_reading[5];
	int SO_MUCH_SWEG = 70;

	while(1){
		//		INSERT WHINING ABOUT DARK SPOT ON THE MAP
		if(rfDataHist[0].Robot_Y > 400){
			SO_MUCH_SWEG = 40;
		}else{
			SO_MUCH_SWEG = 70;
		}


		//		SO_MUCH_SWEG = 75 - rfDataHist[0].Robot_Y/;

		for(counter = 0; counter < 5; counter++){
			last[counter] = new[counter];
		}

		new[0] = channels.front;
		new[1] = channels.far_left + 20;
		new[2] = channels.inner_left;
		new[3] = channels.inner_right;
		new[4] = channels.far_right;

		for(counter = 0; counter < 5; counter++){
			//Take an average blah
			old_check[counter][4]=old_check[counter][3];
			old_check[counter][3]=old_check[counter][2];
			old_check[counter][2]=old_check[counter][1];
			old_check[counter][1]=old_check[counter][0];
			old_check[counter][0] = new_check[counter];

			//difference between new value and the previous value.
			new_check[counter] = new[counter] - last[counter];

			if(new_check[counter] < 0){
				new_check[counter] *= -1;
			}

			if((old_check[counter][4] +old_check[counter][3] +old_check[counter][2] +old_check[counter][1] + old_check[counter][0]+new_check[counter])/6 > SO_MUCH_SWEG){
				single_reading[counter] = 1;
			}else{
				single_reading[counter] = 0;
			}
//			printf("%d\t", (old_check[counter][4] +old_check[counter][3] +old_check[counter][2] +old_check[counter][1] + old_check[counter][0]+new_check[counter])/6);
		}
		LIGHT_STATUS = single_reading[0]*10000+ single_reading[1]*1000+single_reading[2]*100+single_reading[3]*10+single_reading[4];

		if(++lightSensorIndex >= 5)
			lightSensorIndex = 0;


		lightSensor[lightSensorIndex] = (single_reading[0]*128 + single_reading[1]*64+single_reading[2]*32+single_reading[3]*16+single_reading[4]*8+(read & 0b111));
//		printf("\n");
		OSTimeDlyHMSM(0, 0, 0, 10);
	}
}
