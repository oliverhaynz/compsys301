/*
 * logic_task.c
 *
 *  Created on: 18/09/2015
 *      Author: daniel
 */
#include "logic_task.h"
#include "light_task.h"

//Globals_
int Desired_A = 0;
int lastRFIndex = -1;
int lastMovement = 0;
int lastSpeed = 0;
int sumSpeed = 0;
int forwards=0, left=0, right=0;
int adj_r =0;
int adj_l =0;
int ctd_r=0,ctd_l=0;
int timeout = -1, starttime = 0;
float Voltage_multi;
enum {STRAIGHT, LEFT, RIGHT, STOP, AROUND};

void logic_task(void* pdata){

	InitilisePWM();
	int direction;
	int isTurning;
	int firstStop;
	int checkingIntersection;
	int timeCheck, lightCheck;
	int isRunning = 0;

	//Start voltage calculation.
	OSTimeDlyHMSM(0,0,0,30);
	Voltage_multi = 4.8/(channels.Voltage*5.0/4096); //4-5
//	//printf("battery voltate: %f, calculated ofset: %f", channels.Voltage*5.0/4096,Voltage_multi );
	while(1){

		if (!isRunning) {
			// Wait for front to be on line, side off
			if (((lightSensor[0]>>3) & 0b11001) == 0b01001) {
				// Set start flag in astar_task
				start_astar();
				isRunning = 1;
			} else {
				// Sleep until we are in the correct position
				OSTimeDlyHMSM(0,0,0,20);
				continue;
			}
		}
		if (!IsReady) {
			// Reset flags
			isTurning = 0;
			checkingIntersection = 0;
			// Sleep until ready
			OSTimeDlyHMSM(0,0,0,10);
			continue;
		}
		//If the current path value is the head, check our position with RF
		if (!isTurning && !path->backward_len) {
			if (((path->node->type == 3 && (lightSensor[0]>>3)) == 0b11111) || !CheckRFIntersection()) { //CHeck its at a dead end.
				if (CheckTurn()) {
					// Force us out of a head node if we have hit an unexpected intersection
					path->backward_len = -1;
					// Use the intersection code
					checkingIntersection = 1;
				} else {
					// Keep going until we reach the intersection
					FollowLine();
					OSTimeDlyHMSM(0,0,0,10);
					continue;
				}
			} else {
//				//printf("Reached start position\n");
				direction = getDirection();
				//printf("Turning %s\n", direction == LEFT ? "LEFT" : direction == RIGHT ? "RIGHT" : direction == STRAIGHT ? "STRAIGHT" : direction == AROUND ? "AROUND" : "STOP");
				isTurning = 1;
				starttime = (int)clock();
			}
		}
		//Evaluate sensor status
		if(checkingIntersection || (!isTurning && CheckTurn())) {

			if (!starttime) {
				//printf("Started Timing\n");
				checkingIntersection = 1;
				starttime = (int)clock();
			}
			if (!CheckTurn()) {
				//printf("Begin turning @ [%d,%d]\n", path->node->x, path->node->y);
				checkingIntersection = 0;
				// We need to make a turn based on Algorithm
				if(!CheckRFIntersection())
					direction = STOP;
				else
					direction = getDirection();
				firstStop = direction == STOP;
				//printf("Turning %s\n", direction == LEFT ? "LEFT" : direction == RIGHT ? "RIGHT" : direction == STRAIGHT ? "STRAIGHT" : direction == AROUND ? "AROUND" : "STOP");
				isTurning = 1;
				starttime = (int)clock();
				forwards = 0;
				left = 0;
				right = 0;
				timeout = -1;
			} else if ((starttime + 1000) < (int)clock()) {
				// Lost the intersection
				//printf("Timed out\n");
				checkingIntersection = 0;
				starttime = 0;
			}
		}
		// Check if we should be turning
		if(isTurning) {
			if (direction == STOP){ // If we are stopping, stop for longer
				timeCheck = (starttime + 200) < (int)clock();
				if (firstStop && timeCheck) {
					start_astar();
					firstStop = 0;
				}
			}
			else if(direction == AROUND){
				timeCheck = (starttime + 2000) < (int)clock();
			}
			else //Check if we have finished turning
				timeCheck = (starttime + 1000) < (int)clock();
			lightCheck = (lightSensor[0]>>3) & 0b10000; // 10000 = front not on line, 00000 = front on line
			if(timeCheck && (!lightCheck || direction == STRAIGHT)) {
				//Time > 0.5s
				//Forward sensor is on the line
				isTurning = 0;
				direction = STRAIGHT;
				ExecuteTurn(direction);
				// Mark food as collected between the current node and the next
				mark_eaten(path);
				// Continue the A* from the next node
				continue_astar();
				// Increment position
				path = path->forward;
				// Free the previous path from memory
				free(path->backward);
				starttime = 0;
				continue;
			}
			ExecuteTurn(direction);
		} else if (timeCheck && direction == STOP) {
			isTurning = 0;
		} else { //move straight
			FollowLine();
		}
		OSTimeDlyHMSM(0, 0, 0, 10);
	}
}

void FollowLine(){

	left = 0;
	right = 0;
	int r_val =0;
	int l_val =0;
	int stop = 0;
	alt_u8 sense;

	//Get the light sensor output.
	sense = lightSensor[0]>>3;

	//Work out our possible moves, from the light sense circuit.
	if(sense == 0b00000){
		//All sense in dark
		stop = 1;
	}
	if(!(sense & 0b10000)){
		//front sensor is in dark
		forwards = 1;
	}
	if(!(sense & 0b01000)){
		//Can turn left
		left = 1;
	}
	if(!(sense & 0b00001)){
		//Can turn right
		right = 1;
	}

	//Inner sensors triggered: need to adjust path accordingly.
	if(!(sense & 0b00010)){
		//Right
		adj_r = 1;
	}else{
		adj_r = 0;
	}

	if(!(sense & 0b00100)){
		//Left
		adj_l = 1;
	}else{
		adj_l = 0;
	}

	//Motor Adjustments to keep on line, from if statement blocks above
	if(adj_r){
		r_val = -300;
		l_val = 300;
	}
	else if(adj_l){
		r_val = 300;
		l_val = -300;
	}else{
		r_val = 0;
		l_val = 0;
	}
	/*
	//Reached end of line, turn around.
	if(timeout > 0 && (clock()-timeout)>1000){
		motor_movements(1300,-1300);//Turn in circle
		timeout = -1;
	} else
	 */
	//Logic for movement, based on above.
	if(!stop){
		//Always go forwards if you can.
		if(forwards){
			//Resets here to solve for the case where we've been doing a turn,
			left = 0;
			forwards = 0;
			right = 0;
			motor_movements(950+r_val, 950 + l_val);
			timeout = -1;
		}
		//		else if(left && !right){
		//			//Turn Left
		//			motor_movements(1200,-500);
		//			timeout = -1;
		//		}
		//		else if(right && !left){
		//			//Turn right
		//			motor_movements(-500,1200);
		//			timeout = -1;
		//		}
		else{
			//What the current motor speed is.
			motor_movements(1000+r_val,1000 + l_val);
			timeout = -1;
		}
		//Robot has come off, need to spin around.
		if(sense == 0b11111 && timeout < 0){
			timeout = clock();
		}
	}else{
		motor_movements(0,0);
	}
}

void ExecuteTurn(int direction){
	if(direction == LEFT){
		//Turn Left
		motor_movements(1200,-900);
	}
	else if(direction == RIGHT){
		//Turn right
		motor_movements(-900,1200);
	}
	else if (direction == STRAIGHT) {
		// Keep going straight
		// and try to adjust left/right
		if(!(lightSensor[0] && 0b00010)){
			motor_movements(600, 1200);
		}else if(!(lightSensor[0] && 0b00100)){
			motor_movements(1200,600);
		}else{
			motor_movements(950, 950);
		}
	} else if (direction == STOP) {
		// DEBUG
		motor_movements(0, 0);
	}else if(direction == AROUND){
		motor_movements(1200, -1200);
	}else{
		//Make sure the robot goes straight on the dead ends.
		if(!(lightSensor[0] && 0b00010)){
			motor_movements(600, 1200);
		}else if(!(lightSensor[0] && 0b00100)){
			motor_movements(1200,600);
		}else{
			motor_movements(950, 950);
		}
	}
}

int CheckTurn(){
	int sense = lightSensor[0]>>3;

	return !(sense & 0b01000) || !(sense & 0b00001); // Left is on line OR Right is on line
}

int CheckRFIntersection(){
	int x_Astar, y_Astar;
	int x_RF, y_RF;
	int x_diff, y_diff;
	//check astar path is not null (i.e we have already reached the goal)
	if (!path)
		return 0;
	//get current position
	x_RF = rfDataHist[rfDataIndex].Robot_X;
	y_RF = rfDataHist[rfDataIndex].Robot_Y;
	//get next intersection position
	//convert map coordinates to pixel positions
	x_Astar = path->node->x;
	x_Astar *= XMULT;
	x_Astar += XOFFSET;
	y_Astar = path->node->y;
	y_Astar *= YMULT;
	y_Astar += YOFFSET;
	//if these are within 25px of each other, return 1;
	x_diff = x_RF - x_Astar;
	y_diff = y_RF - y_Astar;
	//printf("Comparing [%d,%d] from RF with [%d,%d] from Astar\n", x_RF, y_RF, x_Astar, y_Astar);
	return ((x_diff > -PX_DIST && x_diff < PX_DIST) && (y_diff > -PX_DIST && y_diff < PX_DIST));
}


int getDirection() {
	int dir_Astar;
	int angle, dir_RF = 0b1111;
	int dir_desired;
	int dir_turn;
	// if the current node is the tail then stop moving
	if (!path->forward_len) {
		//printf("Reached tail\n");
		return STOP;
	}
	// get current direction
	dir_Astar = path->backward_dir;
	dir_desired = path->forward_dir;
	angle = rfDataHist[rfDataIndex].Robot_A;
	dir_RF = convert_rf_dir(angle);
	if (dir_Astar != dir_RF) {
		//printf("Direction miss got %d, expected %d\n", dir_RF, dir_Astar);
		return STOP;
	}
	// determine which direction to turn
	//printf("rf: %d, astar: %d, desired: %d\n", dir_RF, dir_Astar, dir_desired);
	//printf("RF Facing %s\n", dir_RF == 0b1000 ? "NORTH" : dir_RF == 0b0100 ? "SOUTH" : dir_RF == 0b0010 ? "EAST" : "WEST");
	//printf("Astar in %s\n", dir_Astar == 0b1000 ? "NORTH" : dir_Astar == 0b0100 ? "SOUTH" : dir_Astar == 0b0010 ? "EAST" : "WEST");
	//printf("Astar out %s\n", dir_desired == 0b1000 ? "NORTH" : dir_desired == 0b0100 ? "SOUTH" : dir_desired == 0b0010 ? "EAST" : "WEST");
	if (dir_desired == 0b1000) { // NORTH
		switch (dir_Astar) {
		case 0b0010:	dir_turn = RIGHT; // EAST
		break; // turn right
		case 0b0001:	dir_turn = LEFT; // WEST
		break; // turn left
		case 0b1000:	dir_turn = AROUND; // NORTH
		break; // turn around
		default:		dir_turn = STRAIGHT; // otherwise
		break; // go straight
		}
	} else if (dir_desired == 0b0100) { // SOUTH
		switch (dir_Astar) {
		case 0b0010:	dir_turn = LEFT; // EAST
		break; // turn left
		case 0b0001:	dir_turn = RIGHT; // WEST
		break; // turn right
		case 0b0100:	dir_turn = AROUND; // SOUTH
		break; // turn around
		default:		dir_turn = STRAIGHT; // otherwise
		break; // go straight
		}
	} else if (dir_desired == 0b0010) { // EAST
		switch (dir_Astar) {
		case 0b1000:	dir_turn = LEFT; // NORTH
		break; // turn left
		case 0b0100:	dir_turn = RIGHT; // SOUTH
		break; // turn right
		case 0b0010:	dir_turn = AROUND; // EAST
		break; // turn around
		default:		dir_turn = STRAIGHT; // otherwise
		break; // go straight
		}
	} else if (dir_desired == 0b0001) { // WEST
		switch (dir_Astar) {
		case 0b1000:	dir_turn = RIGHT; // NORTH
		break; // turn right
		case 0b0100:	dir_turn = LEFT; // SOUTH
		break; // turn left
		case 0b0001:	dir_turn = AROUND; // WEST
		break; // turn around
		default:		dir_turn = STRAIGHT; // otherwise
		break; // go straight
		}
	}
	return dir_turn;
}

void motor_movements(int right, int left){
	ctd_l =left*0.8*GLOBAL_LEFT*Voltage_multi;
	ctd_r= right*0.8*GLOBAL_RIGHT*Voltage_multi;
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, ctd_r);
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, ctd_l);
}

int SpeedControl(void){

	int speed, speed_err, speed_diff;

	speed_err = DesiredSpeed - robot_speed.average;
	sumSpeed += speed_err;
	speed_diff = lastSpeed - robot_speed.average;

	speed = (Kp_speed * speed_err) + (Ki_speed * sumSpeed) + (Kd_speed * speed_diff);
	//printf("Returning speed: %d from errors %d, %d and %d\n", speed, speed_err, sumSpeed, speed_diff);
	lastSpeed = robot_speed.average;
	// Cap speed modifier
	if (speed > MAX_PWM) {
		speed = MAX_PWM;
	} else if (speed < -MAX_PWM) {
		speed = -MAX_PWM;
	}
	return speed;
}

void InitilisePWM(void){
	//Set the initial PWM0 clock divider
	/*
	 *Tperiod=(1/Fclock)x(DivFactror)x(PeriodLen)
	 *FPWM=1/Tperiod
	 *Tpulse=(1/Fclock)x(DivFactror)x(PulseLen)
	 *DC=(PulseLen)/(PeriodLen)x100
	 *
	 */
	//Set PWM modes for high decay
	IOWR_ALTERA_AVALON_PWM_MODE(PWM0_BASE, 0);
	IOWR_ALTERA_AVALON_PWM_MODE(PWM1_BASE, 0);
	//Set the initial PWM0 clock divider
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM0_BASE, 5);
	//Set the duty cycle to a (currently 0)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, 0);
	//Set the initial PWM1 clock divider=
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM1_BASE, 5);
	//Set the duty cycle to a (currently 0 also)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, 0);
	return;
}
