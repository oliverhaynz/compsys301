/*
 * interface.c
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */
#include "interface.h"
#include "includes.h"
#include "magnetic_task.h"
#include "light_task.h"
#include "magnetic_task.h"
#include "rf_task.h"
#include "astar_task.h"

char command = 'n';
char subCommand = 'n';
int SPEED_INTERFACE;
FILE* fp;

void interface(void* pdata){
	int reading = 0;
	int i=0;
	int j=0;
	int x=0;
	int y=0;
	int a = 0;

	while(1){
		if(INTERFACE_MODE == 1){
			fp = fopen("/dev/uart1", "w+");
			fprintf(fp,"\e[1;1H\e[2J");
			fprintf(fp, "\r\n\r\n=============\r\n");
			fprintf(fp, "READING:%.5d\r\n", reading);
			fprintf(fp, "=============\r\n");
			fclose(fp);
			fp = fopen("/dev/uart1", "w+");
			fprintf(fp, "LED STATUS:\t%.5d\r\n", LIGHT_STATUS);
			x = rfDataHist[0].Robot_X;
			x -= 24;
			x /= 5.3;
			y = rfDataHist[0].Robot_Y;
			y -= 23;
			y /= 5.1;
			fprintf(fp, "ROBOT_X:\t%.5d\r\n", x);
			fprintf(fp, "ROBOT_Y:\t%.5d\r\n", y);
			fclose(fp);
			fp = fopen("/dev/uart1", "w+");
			fprintf(fp, "ROBOT_A:\t%.5d\r\n", rfDataHist[0].Robot_A);
			fprintf(fp, "ASTAR_X:\t%d\r\n", path->node->x);
			fprintf(fp, "ASTAR_Y:\t%d\r\n", path->node->y);
			fclose(fp);
			fp = fopen("/dev/uart1", "w+");
			fprintf(fp, "%s", PositionError ? "Position error\n" : "");
			fclose(fp);
			reading++;
			OSTimeDlyHMSM(0, 0, 1, 0);
		}else if(INTERFACE_MODE == 2){
			fp = fopen("/dev/uart1", "w+");
			x = rfDataHist[0].Robot_X - 24;
			x = (x + 53/2 - 1)/53;
			y = rfDataHist[0].Robot_Y - 23;
			y = (y + 51/2 - 1)/51;
			fprintf(fp,"\e[1;1H\e[2J");
			for(i = 0; i<15; i++){
				for(j = 0; j < 19; j++){
					if((i == y) && (j == x))
					{
						fprintf(fp, "\033[22;32m# \033[22;31m");
					}else{
						fprintf(fp,"# ");
					}
				}
				fprintf(fp,"\r\n");
			}
			fclose(fp);
			OSTimeDlyHMSM(0, 0, 0, 100);
		}else{

			OSTimeDlyHMSM(0, 0, 2,0);
		}
	}

}
