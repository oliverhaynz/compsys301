/*
 * rf_task.h
 *
 *  Created on: 18/09/2015
 *      Author: danie
 */

#ifndef RF_TASK_H_
#define RF_TASK_H_

#include <stdio.h>
#include "includes.h"
#include "system.h"
#include <time.h>

void rf_task(void* pdata);
void ToStruct(void);

struct RFData{
	int Index;
	int Address;
	int Robot_X;
	int Robot_Y;
	int Robot_A;
	int Ghost0_X;
	int Ghost0_Y;
	int Ghost0_Speed;
	int Ghost0_A;
	int Ghost1_X;
	int Ghost1_Y;
	int Ghost1_Speed;
	int Ghost1_A;
	int Ghost2_X;
	int Ghost2_Y;
	int Ghost2_Speed;
	int Ghost2_A;
	int RSSI;
	float Time;
} rfDataHist[5];
int rfDataIndex;

//int huns[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

#endif /* RF_TASK_H_ */
