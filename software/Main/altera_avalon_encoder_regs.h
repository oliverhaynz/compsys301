

#ifndef __ALTERA_AVALON_ENCODER_REGS_H__
#define __ALTERA_AVALON_ENCODER_REGS_H__

#include <io.h>

#define IORD_ALTERA_AVALON_ENCODER_POSITION(base)        				IORD(base, 0) 
#define IORD_ALTERA_AVALON_ENCODER_SPEED(base)           				IORD(base, 1) 
#define IORD_ALTERA_AVALON_ENCODER_DIRECTION(base)      				IORD(base, 2) 


#define IOWR_ALTERA_AVALON_ENCODER_PRESCALE_EMULATOR(base, data)        IOWR(base, 3, data)
#define IORD_ALTERA_AVALON_ENCODER_PRESCALE_EMULATOR(base)              IORD(base, 3)

#define IOWR_ALTERA_AVALON_ENCODER_PRESCALE(base, data)  				IOWR(base, 4, data)
#define IORD_ALTERA_AVALON_ENCODER_PRESCALE(base)        				IORD(base, 4)

#define IOWR_ALTERA_AVALON_ENCODER_RESET(base, data)     				IOWR(base, 5, data)

#define IOWR_ALTERA_AVALON_ENCODER_POS_LIMIT(base, data) 				IOWR(base, 6, data)
#define IORD_ALTERA_AVALON_ENCODER_POS_LIMIT(base)       				IORD(base, 6)

#define IOWR_ALTERA_AVALON_ENCODER_NEG_LIMIT(base, data) 				IOWR(base, 7, data)
#define IORD_ALTERA_AVALON_ENCODER_NEG_LIMIT(base) 	  					IORD(base, 7)

#define IOWR_ALTERA_AVALON_ENCODER_1X(base) 		  					IOWR(base, 8, 1)
#define IOWR_ALTERA_AVALON_ENCODER_2X(base) 	    	  				IOWR(base, 8, 2)
#define IOWR_ALTERA_AVALON_ENCODER_4X(base) 		  					IOWR(base, 8, 4)

#define IOWR_ALTERA_AVALON_ENCODER_MODE(base, data)       				IOWR(base, 8, data)
#define IORD_ALTERA_AVALON_ENCODER_MODE(base)       					IORD(base, 8)


#endif /* __ALTERA_AVALON_ENCODER_REGS_H__ */
