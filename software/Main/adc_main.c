#include <stdio.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include <time.h>

float ltime = 0; /* calendar time */

int adc_read(int device){
	float ptime;
	ptime = ltime;
	//Device comes like 0b111
	//Need to shift to 0b0001110000000000
	device = device << 10;
	int reading = 0;
	//Blocking until tx is ready
	while (!(IORD_ALTERA_AVALON_SPI_STATUS(SPI_BASE) & ALTERA_AVALON_SPI_STATUS_TRDY_MSK));
	//Combine device with send command and send data control register
	IOWR_ALTERA_AVALON_SPI_TXDATA(SPI_BASE, (0b1000001100010000 | device));
	//Blocking until rx is ready
	while (!(IORD_ALTERA_AVALON_SPI_STATUS(SPI_BASE) & ALTERA_AVALON_SPI_STATUS_RRDY_MSK));
	//Read a value in from the adc - This will be the previous set of data
	reading = IORD_ALTERA_AVALON_SPI_RXDATA(SPI_BASE);
	//Sequence for getting a timestamp value
	ltime=clock();

	//Data is in twos compliment. The following converts these values
	reading = reading & 0b000111111111100;

	return reading;
}

void adc_init(){

	while (!(IORD_ALTERA_AVALON_SPI_STATUS(SPI_BASE) & ALTERA_AVALON_SPI_STATUS_TRDY_MSK));

	IOWR_ALTERA_AVALON_SPI_TXDATA(SPI_BASE, 0b1111111111111111); //Send dummy command for init
}





