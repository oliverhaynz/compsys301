/*
 ************************
 *AUTHOR: DANIEL PALEY	*
 *AUTHOR: TYRONE SHERWIN*
 ************************
 *--->Please Mark using Nios 13.1<---
 *The following program takes an input array from cmd.h and either performs a command
 *or reads from a sensor input and performs a command accordingly.
 *
 */

// libraries
#include <stdio.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_pwm_regs.h"
#include "altera_avalon_pwm_routines.h"
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include "adc_main.h"


// other header files
#include "cmd.h"
//Keeps Device more straight
#define GLOBAL_RIGHT 1.3
#define GLOBAL_LEFT 1.2

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    task1_stk[TASK_STACKSIZE];
OS_STK    task2_stk[TASK_STACKSIZE];
OS_STK    task3_stk[TASK_STACKSIZE];
OS_STK    task4_stk[TASK_STACKSIZE];
OS_STK    task5_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
#define TASK2_PRIORITY      2
#define TASK3_PRIORITY      3
#define TASK4_PRIORITY      4
#define TASK5_PRIORITY      5

int huns[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int global_mem_flag = 0;
//Initially we brake here.
int mem = 4;

int t1,t2,t3,t4,t5,t6,a=800,b=800,flag7,flag8,t7,t8;
int prev_t3, prev_t6;

double avg_speed = 0, avg_speed2 = 0;
int i_global = 0, j_global=0;
int speed = 0, speed2 = 0;

// global instruction value
int global_instr = 0;

//TASK 1:
void task1(void* pdata)
{
	short i, instr, dip;
	short skip = 0;
	double left, right, incr;
	double on, peak;
	short a = 1200;
	int value;
	//Set the initial PWM0 clock divider
	/*
	 *Tperiod=(1/Fclock)x(DivFactror)x(PeriodLen)
	 *FPWM=1/Tperiod
	 *Tpulse=(1/Fclock)x(DivFactror)x(PulseLen)
	 *DC=(PulseLen)/(PeriodLen)x100
	 *
	 */
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM0_BASE, 5);
	//Set the duty cycle to a (currently 0)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, a);
	//Set the initial PWM1 clock divider=
	IOWR_ALTERA_AVALON_PWM_CLOCK_DIVIDER(PWM1_BASE, 5);
	//Set the duty cycle to a (currently 0 also)
	IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, a);

	//While loop to read from array and output commands to the robot
	while (1) {
		//Check instruction size from the header file
		for(i = 0; i<INSTRUCTION_SIZE; i++) {
			//Read each instruction separately from the 1D array
			instr = INSTRUCTION_CMD[i];
			//Check if the instruction is a manual override
			/*
			 * The following piece of code reads inputs from the light sense
			 * Circuit and outputs a wheel command corresponding to the input from
			 * the sense circuit. (Corrects overshoot from line to a basic level)
			 */
			if(instr == 5){
				instr = global_instr;
				dip = instr & 3;
				//Switch statement checks which sensors are on/off (Using only 3 sensors currentlty)
				instr = instr >> 3;

				peak = 0;
				incr = 1;

				switch(dip){
				case 0:
					//Do nothing, regular for bends
					incr = 1.5;
					break;
				case 1:
					//Case for Broken line
					instr = instr | 0b01001;
					break;
				case 2:
					//For Squiggle line
					incr = 1;
					break;
				case 3:
					//For forwads only
					instr = instr | 0b11111;
					peak = 0.56666;
					break;
				default:
					break;
				}


				switch(instr){
				case 0b00000://All lights off... Break
					instr = 4;
					break;
				case 0b11111://All lights off... Break
					instr = 0;
					break;
				case 0b01111://All lights off... Break
					instr = 0;
					break;


					//Front sensor on
				case 0b11010:
					instr = 2;
					break;
				case 0b11110://Right on only
					instr = 2;
					break;
				case 0b10010:
					instr = 2;
					break;
				case 0b11101://Inner Right on only
					instr = 20;
					break;
				case 0b11100:
					instr = 2;
					break;
				case 0b11000:
					instr = 2;
					break;

					//FS off
				case 0b01110://Right on only
					instr = 2;
					break;
				case 0b00010:
					instr = 2;
					break;
				case 0b01101://Inner Right on only
					//									instr = 20;
					instr = 22;
					break;
				case 0b01100:
					instr = 2;
					break;

					//Front sensor on
				case 0b10101:
					instr = 1;
					break;
				case 0b10111://Left on only
					instr = 1;
					break;
				case 0b10001://Left on only
					instr = 1;
					break;
				case 0b11011://Inner Right on only
					instr = 10;
					break;
				case 0b10011:
					instr = 1;
					break;

					//Front sensor off
				case 0b00111://Left on only
					instr = 1;
					break;
				case 0b00001://Left on only
					instr = 1;
					break;
				case 0b01011://Inner Right on only
					//									instr = 10;
					instr = 11;
					break;
				case 0b00011:
					instr = 1;
					break;



				default:
					instr  = 0;
					break;
				}
			}

			switch(instr) {
			case 0: left = 1;
			right = 1;
			on = 0.6+peak;
			break;
			case 1: left = -1;
			right = 1;
			on = 1*incr;
			break;
			case 2: left = 1;
			right = -1;
			on = 1*incr;
			break;
			case 3: left = -1;
			right = -1;
			on = 1;
			break;
			case 10: left = 0;
			right = 0.8;
			on = 1;
			break;
			case 20: left = 0.8;
			right = 0;
			on = 1;
			break;
			case 11: left = 0.6;
			right = 0.6;
			on = 1;
			break;
			case 22: left = 0.6;
			right = 0.6;
			on = 1;
			break;
			default: left = 1;
			right = 1;
			on = 0;
			break;
			}
			right = right*GLOBAL_RIGHT;
			left = left*GLOBAL_LEFT;
			//Set the pwm's based on the array commands
			IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, a*on*right);
			IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, a*on*left);
			OSTimeDlyHMSM(0, 0, 0, 1);
		}
	}
}

//Task 2: Runs concurrently with task 1 reads sensor outputs and shows LED's based
//on these outputs for debug purposes
void task2(void* pdata)
{
	while (1)
	{
		//Read input from the sense circuit and store in a global variable
		global_instr = IORD_ALTERA_AVALON_PIO_DATA(INPUT_PORT_A_BASE);
		//Show LED's based on what light sense circuit has been triggered
		IOWR_ALTERA_AVALON_PIO_DATA(LED_PIO_BASE, global_instr);
		//	  printf("%d\n", global_instr);
		OSTimeDlyHMSM(0, 0, 0, 1);
	}
}

void task3(void* pdata)
{
	FILE* fp;
	int i = 6;
	int j;
	int val;
	int tens[6] = {1, 10, 100, 1000, 10000, 100000};
	char prompt;
	int k;
	char buffer[6];
	//	  printf("Opening UART");
	fp = fopen("/dev/uart0", "r");
	while(1){
		prompt = getc(fp);
//		printf("%c",prompt);
		if(prompt == ',' || prompt == '\n'){
			k++;
			if(k > 17){
				k = 17;
			}
			// convert buffer and store the value
			val = 0;
			for (j = 0; (i + j) < 6; j++) {
				// convert and store
				val += (int)buffer[i + j] * tens[j];
			}
			i = 6;
			// print
			huns[k] = val;
			//			printf("%d,%d,%d", huns[3],huns[4],huns[5]);

		} else if (prompt >= '0' && prompt <= '9') {
			// decrement i
			i--;
			// load prompt into buffer
			buffer[i] = prompt - '0';
		} else if(prompt == '#'){
			//			printf("\n");
			// reset i
			i = 6;
			k = 0;
		} else if(prompt == '-'){
			//			printf("-");
		}
	}
	fclose(fp);
}

void task4(void* pdata)
{
	float voltage;
	int ram,i,j;
	double ram_percent;
	int speed_array[10];
	int speed_array2[10];

	// Initialise ADC
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);

	while (1)
	{
		// Read voltage from ADC and print it
		voltage = adc_read(0b100);
		voltage = adc_read(0b100);
		voltage = adc_read(0b100);
		voltage = voltage*5.0/4096.0;
		//Read Voltage
		printf("%.1f,", voltage);
		//Read Wheel 1 turns
		printf("%d,", t7);
		//Read Wheel 2 turns
		printf("%d,", t8);
		//Print robot location
		printf("%d,%d,%d,", huns[3],huns[4],huns[5]);
		//Timing

		i_global++;
		speed_array[i_global] =t3;
		avg_speed = 0;

		for(i=0; i <10; i++){
			avg_speed+=speed_array[i];
		}
		avg_speed = avg_speed/10;


		j_global++;
		speed_array2[j_global] =t6;
		avg_speed2 = 0;

		for(j = 0; j <10; j++){
			avg_speed2+=speed_array2[j];
		}
		avg_speed2 = avg_speed2/10;

		if(i_global >9){
			speed = 0;
			i_global = 0;
		}
		if(j_global >9){
			speed2 = 0;
			j_global = 0;
		}

		//Print speed in mm/s
		printf("%d,%d,", (int)(55000/avg_speed),(int)(55000/avg_speed2));

		ram = malloc(1);
		ram_percent = (double)ram/800000;

		printf("%f\n", ram_percent);
		prev_t3 = t3;
		prev_t6 = t6;
		OSTimeDlyHMSM(0, 0, 0, 500);
	}
}


void task5(void* pdata)
{
	double val = 0, val2 = 0;
	double val1 = 0, val3 = 0;

	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);
	adc_init();
	OSTimeDlyHMSM(0, 0, 0, 100);
	while (1)
	{
		val =  adc_read(0b000)/4096.0; //PWM1
		val1 =  adc_read(0b001)/4096.0; //PWM0
		val2 =  adc_read(0b010)/4096.0; //PWM1
		val3 =  adc_read(0b011)/4096.0; //PWM0

		if(val > 0.60){
			t1 = clock();
			t3 = t1 - t2;		//Get the time period for option 1 read
			flag7 = 0;			//Option 2 using flags increment flag on each high.
		}						//1 flag = 55mm (Could implement 27.5 instead)
		else if(val2 > 0.60){
			flag7 = 0;
		}
		else{
			if(!flag7){
				t7++;			//Increment flag here
//				printf("%f\n", val2);
			}
			t2 = t1;
			flag7 = 1;
		}

		if(val1 > 0.60){
			t4 = clock();
			t6 = t4 - t5;
			flag8 = 0;
		}
		else if(val3 > 0.60){
			flag8 = 0;
		}
		else{
			if(!flag8){
				t8++;
			}
			t5 = t4;
			flag8 = 1;
		}

		//		IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM0_BASE, a);
		//		IOWR_ALTERA_AVALON_PWM_DUTY_CYCLE(PWM1_BASE, b);
		OSTimeDlyHMSM(0, 0, 0, 5);
	}
}

/* The main function creates two task and starts multi-tasking */
int main_old(void)
{

	//Set up initial tasks
	OSTaskCreateExt(task1,
			NULL,
			(void *)&task1_stk[TASK_STACKSIZE-1],
			TASK1_PRIORITY,
			TASK1_PRIORITY,
			task1_stk,
			TASK_STACKSIZE,
			NULL,
			0);


	OSTaskCreateExt(task2,
			NULL,
			(void *)&task2_stk[TASK_STACKSIZE-1],
			TASK2_PRIORITY,
			TASK2_PRIORITY,
			task2_stk,
			TASK_STACKSIZE,
			NULL,
			0);


	OSTaskCreateExt(task3,
			NULL,
			(void *)&task3_stk[TASK_STACKSIZE-1],
			TASK3_PRIORITY,
			TASK3_PRIORITY,
			task3_stk,
			TASK_STACKSIZE,
			NULL,
			0);


	OSTaskCreateExt(task4,
			NULL,
			(void *)&task4_stk[TASK_STACKSIZE-1],
			TASK4_PRIORITY,
			TASK4_PRIORITY,
			task4_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	OSTaskCreateExt(task5,
			NULL,
			(void *)&task5_stk[TASK_STACKSIZE-1],
			TASK5_PRIORITY,
			TASK5_PRIORITY,
			task5_stk,
			TASK_STACKSIZE,
			NULL,
			0);

	//Begin the operating system
	OSStart();
	return 0;
}
