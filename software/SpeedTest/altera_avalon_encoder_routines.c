
#include "altera_avalon_encoder_routines.h"



//---------------------------------------------------------------
int encoder_position(unsigned int address)
{	
    return(IORD_ALTERA_AVALON_ENCODER_POSITION(address));
}
//---------------------------------------------------------------
//---------------------------------------------------------------
int encoder_speed(unsigned int address)
{	
    return(IORD_ALTERA_AVALON_ENCODER_SPEED(address));
}
//---------------------------------------------------------------
int encoder_dir(unsigned int address)
{	
    return(IORD_ALTERA_AVALON_ENCODER_DIRECTION(address));
}
//---------------------------------------------------------------
//---------------------------------------------------------------
int encoder_prescale_emulator_write(unsigned int address, unsigned int prescale)
{	
    IOWR_ALTERA_AVALON_ENCODER_PRESCALE_EMULATOR(address, prescale);
    if (IORD_ALTERA_AVALON_ENCODER_PRESCALE(address) != prescale)
      return ALTERA_AVALON_ENCODER_REGISTER_SET_ERROR;
    return ALTERA_AVALON_ENCODER_OK; 
}
//---------------------------------------------------------------
int encoder_prescale_emulator_read(unsigned int address)
{   
    return IORD_ALTERA_AVALON_ENCODER_PRESCALE(address);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
int encoder_prescale_write(unsigned int address, unsigned int prescale)
{	
    IOWR_ALTERA_AVALON_ENCODER_PRESCALE(address, prescale);
    if (IORD_ALTERA_AVALON_ENCODER_PRESCALE(address) != prescale)
      return ALTERA_AVALON_ENCODER_REGISTER_SET_ERROR;
    return ALTERA_AVALON_ENCODER_OK; 
}
//---------------------------------------------------------------
int encoder_prescale_read(unsigned int address)
{   
    return IORD_ALTERA_AVALON_ENCODER_PRESCALE(address);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
void encoder_reset(unsigned int address)
{	
    IOWR_ALTERA_AVALON_ENCODER_RESET(address, 1);
    IOWR_ALTERA_AVALON_ENCODER_RESET(address, 0);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
int encoder_pos_limit_write(unsigned int address, int limit)
{	
    IOWR_ALTERA_AVALON_ENCODER_POS_LIMIT(address, limit);
    if (IORD_ALTERA_AVALON_ENCODER_POS_LIMIT(address) != limit)
      return ALTERA_AVALON_ENCODER_REGISTER_SET_ERROR;
    return ALTERA_AVALON_ENCODER_OK;     
}
//---------------------------------------------------------------
int encoder_pos_limit_read(unsigned int address)
{   
    return IORD_ALTERA_AVALON_ENCODER_POS_LIMIT(address);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
int encoder_neg_limit_write(unsigned int address, int limit)
{	
    IOWR_ALTERA_AVALON_ENCODER_NEG_LIMIT(address, limit);
    if (IORD_ALTERA_AVALON_ENCODER_NEG_LIMIT(address) != limit)
      return ALTERA_AVALON_ENCODER_REGISTER_SET_ERROR;
    return ALTERA_AVALON_ENCODER_OK;     
}
//---------------------------------------------------------------
int encoder_neg_limit_read(unsigned int address)
{	
    return IORD_ALTERA_AVALON_ENCODER_NEG_LIMIT(address);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
void encoder_1xmode(unsigned int address)
{
    IOWR_ALTERA_AVALON_ENCODER_1X(address);
     
}
//---------------------------------------------------------------
void encoder_2xmode(unsigned int address)
{
    IOWR_ALTERA_AVALON_ENCODER_2X(address); 
}
//---------------------------------------------------------------
void encoder_4xmode(unsigned int address)
{
    IOWR_ALTERA_AVALON_ENCODER_4X(address); 
}
//---------------------------------------------------------------
int encoder_mode_read(unsigned int address)
{   
    return IORD_ALTERA_AVALON_ENCODER_MODE(address);
}
//---------------------------------------------------------------
//---------------------------------------------------------------
