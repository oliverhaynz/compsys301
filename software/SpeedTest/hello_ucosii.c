#include <stdio.h>
#include <time.h>
#include <string.h>
#include "includes.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_pwm_regs.h"
#include "altera_avalon_pwm_routines.h"
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include "adc_main.h"
#include "cmd.h"

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    task1_stk[TASK_STACKSIZE];
OS_STK    task2_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
#define TASK2_PRIORITY      2

int huns[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
/* Prints "Hello World" and sleeps for three seconds */
void task1(void* pdata)
{
	FILE* fp;
	int i = 6;
	int j;
	int val;
	int tens[6] = {1, 10, 100, 1000, 10000, 100000};
	char prompt;
	int k;
	char buffer[6];
	//	  printf("Opening UART");
	fp = fopen("/dev/uart0", "r");
	while(1){
		prompt = getc(fp);
		if(prompt == ',' || prompt == '\n'){
			k++;
			if(k > 17){
				k = 17;
			}
			// convert buffer and store the value
			val = 0;
			for (j = 0; (i + j) < 6; j++) {
				// convert and store
				val += (int)buffer[i + j] * tens[j];
			}
			i = 6;
			// print
			huns[k] = val;
//			printf("%d,%d,%d", huns[3],huns[4],huns[5]);

		} else if (prompt >= '0' && prompt <= '9') {
			// decrement i
			i--;
			// load prompt into buffer
			buffer[i] = prompt - '0';
		} else if(prompt == '#'){
//			printf("\n");
			// reset i
			i = 6;
			k = 0;
		} else if(prompt == '-'){
//			printf("-");
		}
	}
	fclose(fp);
}
/* Prints "Hello World" and sleeps for three seconds */
void task2(void* pdata)
{
	while (1)
	{
		printf("%d,%d,%d\n", huns[3],huns[4],huns[5]);
		OSTimeDlyHMSM(0, 0, 0, 10);
	}
}
/* The main function creates two task and starts multi-tasking */
int main(void)
{

	OSTaskCreateExt(task1,
			NULL,
			(void *)&task1_stk[TASK_STACKSIZE-1],
			TASK1_PRIORITY,
			TASK1_PRIORITY,
			task1_stk,
			TASK_STACKSIZE,
			NULL,
			0);


	OSTaskCreateExt(task2,
			NULL,
			(void *)&task2_stk[TASK_STACKSIZE-1],
			TASK2_PRIORITY,
			TASK2_PRIORITY,
			task2_stk,
			TASK_STACKSIZE,
			NULL,
			0);
	OSStart();
	return 0;
}
