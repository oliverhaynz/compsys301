
#include "altera_avalon_pwm_regs.h"

#define ALTERA_AVALON_PWM_TYPE (volatile unsigned int*)

int altera_avalon_pwm_init(unsigned int address, unsigned int clock_divider, unsigned int duty_cycle);
//int altera_avalon_pwm_enable(unsigned int address);
//int altera_avalon_pwm_disable(unsigned int address);
int altera_avalon_pwm_change_duty_cycle(unsigned int address, unsigned int duty_cycle);
//int altera_avalon_pwm_mode(unsigned int address, unsigned int mode);
//int altera_avalon_pwm_locked_antiphase_mode(unsigned int address);
//int altera_avalon_pwm_sign_magnitude_mode(unsigned int address);

//Return Codes
#define ALTERA_AVALON_PWM_OK                                          	0
#define ALTERA_AVALON_PWM_REGISTER_SET_ERROR                         	-1
#define ALTERA_AVALON_PWM_ENABLED_CONFIRMATION_ERROR 	              	-2
#define ALTERA_AVALON_PWM_DISABLED_CONFIRMATION_ERROR 	              	-3
#define ALTERA_AVALON_PWM_MODE_CONFIRMATION_ERROR 	              		-4

//Constants
#define ALTERA_AVALON_PWM_ENABLED  1
#define ALTERA_AVALON_PWM_DISABLED 0

#define ALTERA_AVALON_PWM_LOCKED_ANTIPHASE_MODE 0
#define ALTERA_AVALON_PWM_SIGN_MAGNITUDE_MODE   1
